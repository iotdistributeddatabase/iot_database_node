/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_USERSERVER_HPP
#define REPLICATIONSERVICE_USERSERVER_HPP

#include <QTcpServer>
#include <WorkerPool.hpp>
#include <mutex>

namespace ReplicationService
{

class ConnectionNotifier;

class ReplicationServer : public QTcpServer
{
    typedef uint64_t NodeId;
public:
    ReplicationServer() = delete;
    ReplicationServer(const ReplicationServer&) = delete;
    ReplicationServer& operator=(const ReplicationServer&) = delete;
    ReplicationServer(ReplicationServer&&) = delete;
    ReplicationServer& operator=(ReplicationServer&&) = delete;
    explicit ReplicationServer(const QHostAddress &address,
                               const quint16 port,
                               const std::size_t max_threads,
                               const NodeId nodeId);
    bool startServer();
    bool joinDatabaseNode(const QHostAddress& address, const quint16 port);
    bool stopServer();
    ~ReplicationServer() override;


protected:
    void incomingConnection(qintptr socketDescriptor) override;
    QObject* connectToDatabaseNode(const QHostAddress& address, const quint16 port);

public:
    static NodeId globalNodeId;
    static QHostAddress globalAddress;
    static quint16 globalPort;

protected:
    const QHostAddress address_;
    const quint16 port_;
    std::unique_ptr<ConnectionNotifier> connNotifier_;
    Parallel::WorkerPool workerPool_;
};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_USERSERVER_HPP
