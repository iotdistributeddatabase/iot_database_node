/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_TABLEMANAGERS_HPP
#define REPLICATIONSERVICE_TABLEMANAGERS_HPP

#include <string>
#include <unordered_map>
#include <memory>
#include "TableController.hpp"

namespace ReplicationService
{

class TableContainer
{
public:
    typedef std::string TableNameStrKey;
    typedef std::shared_ptr<TableController> TableControllerPtr;

    explicit TableContainer() = default;
    TableContainer(const TableContainer&) = delete;
    TableContainer& operator=(const TableContainer&) = delete;
    TableContainer(TableContainer&&) = delete;
    TableContainer& operator=(TableContainer&&) = delete;

    TableControllerPtr getTable(const TableNameStrKey& name);
    TableControllerPtr getOrIfNotExistAddTable(const TableNameStrKey& name);

    static TableContainer& globalInstance();

private:
    std::mutex mutex_;
    std::unordered_map<TableNameStrKey, TableControllerPtr> tables_;

};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_TABLEMANAGERS_HPP
