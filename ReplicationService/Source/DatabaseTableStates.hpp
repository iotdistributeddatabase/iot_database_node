/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_DATABASETABLESTATES_HPP
#define REPLICATIONSERVICE_DATABASETABLESTATES_HPP

#include <unordered_map>
#include <string>
#include <cstdint>
#include <mutex>
#include <IoTDNPMsgPrototypes.hpp>

#include <TransactionId.hpp>

namespace ReplicationService
{

class DatabaseTableStates
{
    typedef uint64_t TableTransId;
public:
    explicit DatabaseTableStates() = default;
    DatabaseTableStates(const DatabaseTableStates&) = delete;
    DatabaseTableStates& operator=(const DatabaseTableStates&) = delete;
    DatabaseTableStates(DatabaseTableStates&&) = delete;
    DatabaseTableStates& operator=(DatabaseTableStates&&) = delete;

    void incrementTableState(const std::string& table);
    bool insertTableState(const std::string& table, const TableTransId tableTransId);
    ::IoTDNP::TransactionIdStruct saveTableStatesToMsg(Msg::IoTDNP::FetchDatabaseStateRespMsgBuilder* msg);

    static DatabaseTableStates& globalInstance();

private:
    std::mutex mutex_;
    std::unordered_map<std::string, TableTransId> tableStates_;
    TransactionId newestTransactionId_;
};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_DATABASETABLESTATES_HPP
