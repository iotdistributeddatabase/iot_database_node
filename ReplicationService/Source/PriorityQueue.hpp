/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_PRIORITY_QUEUE_HPP
#define REPLICATIONSERVICE_PRIORITY_QUEUE_HPP

#include <queue>
#include <algorithm>

namespace ReplicationService
{

template<typename T, typename Container, typename Compare>
class PriorityQueue : public std::priority_queue<T, Container, Compare>
{
  public:
    void erase(const T& value)
    {
        auto it = std::find(this->c.begin(), this->c.end(), value);
        if (it != this->c.end())
        {
            this->c.erase(it);
            std::make_heap(this->c.begin(), this->c.end(), this->comp);
        }
    }
};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_PRIORITY_QUEUE_HPP
