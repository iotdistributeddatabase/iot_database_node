/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TransactionId.hpp"
#include <ReplicationServer.hpp>
#include <QDateTime>

namespace ReplicationService
{

bool operator==(const TransactionId& lhs, const TransactionId& rhs) noexcept
{
    return (lhs.timeMsSinceEpoch == rhs.timeMsSinceEpoch)
            && (lhs.nodeId == rhs.nodeId)
            && (lhs.nodeCounter == rhs.nodeCounter);
}

QDebug operator<< (QDebug d, const TransactionId& id) {
    d << "TransactionId{timeMsSinceEpoch=" << id.timeMsSinceEpoch
      << ", nodeId=" << id.nodeId <<  ", nodeCounter=" << id.nodeCounter << "}";
    return d;
}

TransactionId makeTransactionId()
{
    static std::atomic_uint_fast64_t nodeCounter = 0;
    return TransactionId {QDateTime::currentMSecsSinceEpoch(),
                          ReplicationServer::globalNodeId, ++nodeCounter};
}

TransactionId makeTransactionId(const IoTDNP::TransactionIdStruct& id)
{
    return TransactionId {id.timeMsSinceEpoch(), id.nodeId(), id.nodeCounter()};
}

TransactionId makeTransactionId(const IoTDNP::TransactionIdStruct* id)
{
    return TransactionId {id->timeMsSinceEpoch(), id->nodeId(), id->nodeCounter()};
}

TransactionId makeTransactionId(const int64_t timeMsSinceEpoch, const uint64_t nodeId,
                                const uint64_t nodeCounter)
{
    return TransactionId {timeMsSinceEpoch, nodeId, nodeCounter};
}

IoTDNP::TransactionIdStruct makeTransactionIdStruct(const TransactionId& id)
{
    return IoTDNP::TransactionIdStruct {id.timeMsSinceEpoch, id.nodeId, id.nodeCounter};
}

} // namespace ReplicationService
