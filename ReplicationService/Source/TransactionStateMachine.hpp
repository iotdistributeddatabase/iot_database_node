/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_TRANSACTIONSTATEMACHINE_HPP
#define REPLICATIONSERVICE_TRANSACTIONSTATEMACHINE_HPP

#include "ReplicationServiceTypes.hpp"
#include <boost/sml.hpp>
#include <QDebug>
#include <type_traits>

namespace sml = boost::sml;

namespace ReplicationService
{
namespace StateMachine
{

// Events

struct ReadyNode
{
    Nodes nodes;
};

struct StopNode
{
    Nodes nodes;
};

struct AbortNode
{
    Nodes nodes;
};

struct CrashNode
{
    Nodes nodes;
};

struct UpdateNodes
{
};

struct Finish
{
};

// State Machine

struct TransactionState
{
    Nodes nodes;
    Nodes receivedNodes;
};

class TransactionSM
{
public:
    /************ States ************/
    static constexpr auto Init = sml::state<class Init>;
    static constexpr auto Ready = sml::state<class Ready>;
    static constexpr auto Readied = sml::state<class Readied>;
    static constexpr auto NotReady = sml::state<class NotReady>;
    static constexpr auto NotReadied = sml::state<class NotReadied>;
    static constexpr auto Commit = sml::state<class Commit>;
    static constexpr auto Commited = sml::state<class Commited>;
    static constexpr auto NotCommit = sml::state<class NotCommit>;
    static constexpr auto NotCommited = sml::state<class NotCommited>;
    static constexpr auto Aborted = sml::state<class Aborted>;

    auto operator()() const noexcept
    {
        return sml::make_transition_table(
                *Init + ReadyNodeEvent [ IsReady ] = Ready,
                *Init + UpdateNodesEvent [ IsReady ] = Ready,
                *Init + CrashNodeEvent [ IsReady ] = Ready,
                *Init + AbortNodeEvent / Aborting = NotReady,
                NotReady + ReadyNodeEvent [ IsNotReadied ] = NotReadied,
                NotReady + AbortNodeEvent [ IsNotReadied ] = NotReadied,
                NotReady + CrashNodeEvent [ IsNotReadied ] = NotReadied,
                NotReady + FinishEvent = NotReadied,
                NotReadied + FinishEvent = sml::X,
                Ready + FinishEvent / Readying = Readied,
                Readied + StopNodeEvent [ IsCommit ] = Commit,
                Readied + CrashNodeEvent [ IsCommit ] = Commit,
                Readied + AbortNodeEvent / Aborting = NotCommit,
                NotCommit + StopNodeEvent [ IsNotCommited ] = NotCommited,
                NotCommit + CrashNodeEvent [ IsNotCommited ] = NotCommited,
                NotCommit + AbortNodeEvent [ IsNotCommited ] = NotCommited,
                NotCommit + FinishEvent = NotCommited,
                NotCommited + FinishEvent = sml::X,
                Commit + FinishEvent = Commited,
                Commited + FinishEvent = sml::X
                                         );
    }

private:
    /************ Events ************/
    static constexpr auto ReadyNodeEvent = sml::event<ReadyNode>;
    static constexpr auto AbortNodeEvent = sml::event<AbortNode>;
    static constexpr auto CrashNodeEvent = sml::event<CrashNode>;
    static constexpr auto StopNodeEvent = sml::event<StopNode>;
    static constexpr auto UpdateNodesEvent = sml::event<UpdateNodes>;
    static constexpr auto FinishEvent = sml::event<Finish>;

    /************ Guards ************/
    struct UpdateState
    {
        template <typename T>
        void operator()(TransactionState& s, const T& e)
        {
            s.receivedNodes |= e.nodes;
        }
    };
    struct IsTransition
    {
        bool operator()(TransactionState& s)
        {
            return (s.nodes ^ s.receivedNodes) == 0;
        }
        template<typename T, typename U = int>
        struct HasNodes : std::false_type {};
        template<typename T>
        struct HasNodes<T, decltype((void) T::nodes, 0)> : std::true_type {};
        template <typename T, std::enable_if_t<not HasNodes<T>::value, int> = 0>
        bool operator()(TransactionState& s, const T&)
        {
            return operator()(s);
        }
        template <typename T, std::enable_if_t<HasNodes<T>::value, int> = 0>
        bool operator()(TransactionState& s, const T& e)
        {
            UpdateState{}(s, e);
            return operator()(s);
        }
    };
    struct IsReadyGuard : IsTransition {};
    struct IsNotReadiedGuard : IsTransition {};
    struct IsCommitGuard : IsTransition {};
    struct IsNotCommitedGuard : IsTransition {};
    static constexpr auto IsReady = IsReadyGuard{};
    static constexpr auto IsNotReadied = IsNotReadiedGuard{};
    static constexpr auto IsCommit = IsCommitGuard{};
    static constexpr auto IsNotCommited = IsNotCommitedGuard{};

    /************ Actions ************/
    struct AbortingAction
    {
        void operator()(TransactionState& s, const AbortNode& e)
        {
            UpdateState{}(s, e);
        }
    };
    struct ReadyingAction
    {
        void operator()(TransactionState& s)
        {
            s.receivedNodes = 0;
        }
    };
    static constexpr auto Aborting = AbortingAction{};
    static constexpr auto Readying = ReadyingAction{};
};

struct DebugLogger
{
    template <class SM, class TEvent>
    void log_process_event(const TEvent&) const
    {
        qDebug("[%s][process_event] %s", sml::aux::get_type_name<SM>(),
               sml::aux::get_type_name<TEvent>());
    }

    template <class SM, class TGuard, class TEvent>
    void log_guard(const TGuard&, const TEvent&, bool result) const
    {
        qDebug("[%s][guard] %s %s %s", sml::aux::get_type_name<SM>(),
               sml::aux::get_type_name<TGuard>(),
               sml::aux::get_type_name<TEvent>(),
               (result ? "[OK]" : "[Reject]"));
    }

    template <class SM, class TAction, class TEvent>
    void log_action(const TAction&, const TEvent&) const
    {
        qDebug("[%s][action] %s %s", sml::aux::get_type_name<SM>(),
               sml::aux::get_type_name<TAction>(),
               sml::aux::get_type_name<TEvent>());
    }

    template <class SM, class TSrcState, class TDstState>
    void log_state_change(const TSrcState& src, const TDstState& dst) const
    {
        qDebug("[%s][transition] %s -> %s", sml::aux::get_type_name<SM>(),
               src.c_str(), dst.c_str());
    }
};

} // namespace StateMachine
} // namespace ReplicationService

QDebug operator<<(QDebug d, const ReplicationService::StateMachine::TransactionState& s);

#endif // REPLICATIONSERVICE_TRANSACTIONSTATEMACHINE_HPP
