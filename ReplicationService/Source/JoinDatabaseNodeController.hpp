/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_JOINDATABASENODECONTROLLER_HPP
#define REPLICATIONSERVICE_JOINDATABASENODECONTROLLER_HPP


#include "TransactionController.hpp"
#include "ReplicationServiceTypes.hpp"

#include <mutex>

namespace ReplicationService
{

class JoinDatabaseNodeController : public TransactionController
{
public:
    JoinDatabaseNodeController(const JoinDatabaseNodeController&) = delete;
    JoinDatabaseNodeController& operator=(const JoinDatabaseNodeController&) = delete;
    JoinDatabaseNodeController(JoinDatabaseNodeController&&) = delete;
    JoinDatabaseNodeController& operator=(JoinDatabaseNodeController&&) = delete;
    explicit JoinDatabaseNodeController();
    explicit JoinDatabaseNodeController(const NodeId ownNodeId,
                                        const NodeId joinNodeId,
                                        const ::IoTDNP::DatabaseNodeAddrStruct* addr);

    bool setTransaction(const Nodes nodes, const NodeId readyNode,
                        const NodeId ownNodeId, const NodeId joinNodeId,
                        const IoTDNP::DatabaseNodeAddrStruct* addr);

    const IoTDNP::DatabaseNodeAddrStruct& addr() const;
    NodeId joinNodeId() const;

private:
    NodeId joinNodeId_;
    ::IoTDNP::DatabaseNodeAddrStruct addr_;
};
} // namespace ReplicationService

#endif // REPLICATIONSERVICE_JOINDATABASENODECONTROLLER_HPP
