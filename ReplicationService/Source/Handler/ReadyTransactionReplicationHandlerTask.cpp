/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ReadyTransactionReplicationHandlerTask.hpp"
#include "DatabaseTransactionHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

ReadyTransactionReplicationHandlerTask::ReadyTransactionReplicationHandlerTask(
        const char *tableName)
    : tableName_(tableName)
{
}

void ReadyTransactionReplicationHandlerTask::run()
{
    auto table = TableContainer::globalInstance()
            .getTable(tableName_);

    topReadyTransaction(this, table, tableName_.c_str());
}

} // namespace Handler
} // namespace ReplicationService
