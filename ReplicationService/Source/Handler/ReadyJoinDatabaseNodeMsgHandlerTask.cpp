/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ReadyJoinDatabaseNodeMsgHandlerTask.hpp"
#include <JoinDatabaseNodeController.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include "JoinDatabaseNodeHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

ReadyJoinDatabaseNodeMsgHandlerTask::ReadyJoinDatabaseNodeMsgHandlerTask(MsgHandleType&& msg)
    : msg_(std::move(msg))
{
}

void ReadyJoinDatabaseNodeMsgHandlerTask::run()
{
    auto trans = JoinDatabaseNodeQueue::globalInstance().getOrAddIfNotExistTransaction(
                makeTransactionId(msg_->transactionId()));

    if(trans->setReadyNode(msg_->nodeId(), msg_->status()))
    {
        topReadyTransaction(this);
    }
    qInfo() << "handle ReadyJoinDatabaseNodeMsg with nodeId=" << msg_->nodeId()
            << "status=" << ::IoTDNP::EnumNameReadyStatusEnum(msg_->status());
}

void ReadyJoinDatabaseNodeMsgHandlerTask::emitReadyWrite(IMsgPtr msg)
{
    emit readyWrite(msg);
}

void ReadyJoinDatabaseNodeMsgHandlerTask::emitAddConnection(IConnectionInitiatorPtr init)
{
    emit addConnection(init);
}

} // namespace Handler
} // namespace ReplicationService
