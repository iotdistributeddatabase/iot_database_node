/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_HANDLER_DATABASETRANSACTIONHELPER_HPP
#define REPLICATIONSERVICE_HANDLER_DATABASETRANSACTIONHELPER_HPP

#include <TableContainer.hpp>
#include <TransactionController.hpp>
#include <IoTDNPMsgPrototypes.hpp>

#include <ReplicationServer.hpp>
#include <MessageTypes.hpp>
#include <ConnectionContainer.hpp>
#include <DatabaseTableStates.hpp>

namespace ReplicationService
{
namespace Handler
{

template <typename Sender>
inline void popCommitTransaction(Sender* sender,
                                 TableContainer::TableControllerPtr table,
                                 const char* tableName);

template <typename Sender>
inline void topReadyTransaction(Sender* sender,
                                TableContainer::TableControllerPtr table,
                                const char* tableName)
{
    if(auto [id, transaction] = table->topReadyTransaction(); transaction)
    {
        if(transaction->isReadied())
        {
            sender->disconnect();

            ::IoTDNP::CommitStatusEnum status = transaction->readyTransaction();

            ConnectionContainer::globalInstance().connectRecivers(
                        sender, SIGNAL(readyWrite(IMsgPtr)), transaction->nodes());

            IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::StopTransactionMsgPrototype>(
                        tableName, makeTransactionIdStruct(id),
                        ReplicationServer::globalNodeId,
                        status);

            emit sender->readyWrite(msg);

            if(transaction->setStopNode(ReplicationServer::globalNodeId, status))
            {
                popCommitTransaction(sender, table, tableName);
            }
        }
        else
        {
            table->popTransaction();
            topReadyTransaction(sender, table, tableName);
        }
    }
}

template <typename Sender>
inline void popCommitTransaction(Sender* sender,
                                 TableContainer::TableControllerPtr table,
                                 const char* tableName)
{
    if(auto [id, transaction] = table->popCommitTransaction(); transaction)
    {
//        qInfo() << "popCommitTransaction: isCommited=" << transaction->isCommited();
        if(transaction->isCommited())
        {
            if(transaction->commitTransaction())
            {
                DatabaseTableStates::globalInstance()
                        .incrementTableState(tableName);
            }
            else
            {
                transaction->ifPresentHandleTransFinish(false, "crash");
                //TODO: send msg crashed
            }
        }

        transaction.reset(); // speed up free memory

        topReadyTransaction(sender, table, tableName);
    }
}


} // namespace ReplicationService
} // namespace Handler

#endif // REPLICATIONSERVICE_HANDLER_DATABASETRANSACTIONHELPER_HPP
