/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StopJoinDatabaseNodeMsgHandlerTask.hpp"
#include <JoinDatabaseNodeController.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include "JoinDatabaseNodeHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

StopJoinDatabaseNodeMsgHandlerTask::StopJoinDatabaseNodeMsgHandlerTask(
        MsgHandleType&& msg)
    : msg_(std::move(msg))
{
}

void StopJoinDatabaseNodeMsgHandlerTask::run()
{
    auto trans = JoinDatabaseNodeQueue::globalInstance().getTransaction(
                makeTransactionId(msg_->transactionId()));

    if(trans->setStopNode(msg_->nodeId(), msg_->status()))
    {
        popCommitTransaction(this);
    }
    qInfo() << "handle StopJoinDatabaseNodeMsg with nodeId=" << msg_->nodeId()
             << "status=" << ::IoTDNP::EnumNameCommitStatusEnum(msg_->status());
}

void StopJoinDatabaseNodeMsgHandlerTask::emitReadyWrite(IMsgPtr msg)
{
    emit readyWrite(msg);
}

void StopJoinDatabaseNodeMsgHandlerTask::emitAddConnection(IConnectionInitiatorPtr init)
{
    emit addConnection(init);
}

} // namespace Handler
} // namespace ReplicationService
