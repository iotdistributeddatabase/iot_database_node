/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_HANDLER_READYJOINDATABASENODEMSGHANDLERTASK_HPP
#define REPLICATIONSERVICE_HANDLER_READYJOINDATABASENODEMSGHANDLERTASK_HPP

#include <Util/Handler/HandlerTask.hpp>
#include <MessageHandle.hpp>
#include <MessageBuffer.hpp>
#include <MessageTypes.hpp>
#include <IoTDNP_generated.h>
#include <IConnectionInitiator.hpp>
#include <QObject>

namespace ReplicationService
{
namespace Handler
{

class ReadyJoinDatabaseNodeMsgHandlerTask : public QObject,
                                            public Util::Handler::HandlerTask
{
    Q_OBJECT
public:
    using MsgType = ::IoTDNP::ReadyJoinDatabaseNodeMsg;
    using MsgHandleType = Msg::MessageHandle<MsgType>;
    explicit ReadyJoinDatabaseNodeMsgHandlerTask(MsgHandleType&& msg);

    void run() override;
    void emitReadyWrite(IMsgPtr msg);
    void emitAddConnection(IConnectionInitiatorPtr init);

signals:
    void readyWrite(IMsgPtr msg);
    void addConnection(IConnectionInitiatorPtr init);

public slots:

private:
    const MsgHandleType msg_;
};

} // namespace Handler
} // namespace ReplicationService

#endif // REPLICATIONSERVICE_HANDLER_READYJOINDATABASENODEMSGHANDLERTASK_HPP
