/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FetchDatabaseTableRespMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include <DbConnectionPool.hpp>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QByteArray>
#include <QDataStream>
#include <QVariant>
#include <QVariantList>

namespace ReplicationService
{
namespace Handler
{

FetchDatabaseTableRespMsgHandlerTask::FetchDatabaseTableRespMsgHandlerTask(
        MsgHandleType&& msg,
        const ReplicationConnectionManager *connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{
}

void FetchDatabaseTableRespMsgHandlerTask::run()
{
    QString table {msg_->table()->c_str()};
    QString values {"?"};
    for (std::size_t i = 1; i < msg_->columns()->size(); ++i)
    {
        values += ", ?";
    }
    auto db = Database::DbConnectionPool::globalInstance()->getConnection();
    QSqlQuery query {db};
    bool result = query.prepare("INSERT INTO " + table +
                                " VALUES (" + values + ")");
    QByteArray dataRecords {reinterpret_cast<const char*>(msg_->records()->Data()),
                            static_cast<int>(msg_->records()->size())};
    QDataStream dataStreamRecords {&dataRecords, QIODevice::ReadOnly};
    QVariant filedVariant {};
    QVariantList record {};
    for (uint64_t i = 0; i < msg_->rows(); ++i)
    {
        for (std::size_t j = 0; j < msg_->columns()->size(); ++j)
        {
            dataStreamRecords >> filedVariant;
            record << filedVariant;
        }
        query.addBindValue(record);
        record.clear();
    }

    if(not result or not query.execBatch(QSqlQuery::ValuesAsColumns))
    {
        qCritical() << "FetchDatabaseTableRespMsgHandlerTask could not save table="
                    << msg_->table() << " db.lastError=" << db.lastError().text();
    }
}

} // namespace Handler
} // namespace ReplicationService
