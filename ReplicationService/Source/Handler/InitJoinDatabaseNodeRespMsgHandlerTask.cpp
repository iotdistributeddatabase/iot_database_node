/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "InitJoinDatabaseNodeRespMsgHandlerTask.hpp"
#include <ConnectionContainer.hpp>

namespace ReplicationService
{
namespace Handler
{

InitJoinDatabaseNodeRespMsgHandlerTask::InitJoinDatabaseNodeRespMsgHandlerTask(
        MsgHandleType&& msg,
        const ReplicationConnectionManager* connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{
}

void InitJoinDatabaseNodeRespMsgHandlerTask::run()
{
    const ConnectionInfo conn = makeConnectionInfo(connManager_, msg_->nodeId(),
                                                   msg_->addr());

    ConnectionContainer::globalInstance().addConnection(conn);
    qInfo() << "handle InitJoinDatabaseNodeRespMsg with nodeId=" << msg_->nodeId();
}

} // namespace Handler
} // namespace ReplicationService
