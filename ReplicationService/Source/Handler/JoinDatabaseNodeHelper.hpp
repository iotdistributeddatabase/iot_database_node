/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_HANDLER_JOINDATABASENODEHELPER_HPP
#define REPLICATIONSERVICE_HANDLER_JOINDATABASENODEHELPER_HPP

#include <AddConnectionJoinDatabaseNodeInitiator.hpp>
#include <IConnectionInitiator.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include <MessageTypes.hpp>
#include <IoTDNPMsgPrototypes.hpp>

#include <ReplicationServer.hpp>
#include <ConnectionContainer.hpp>
#include <ConnectionNotifier.hpp>

namespace ReplicationService
{
namespace Handler
{

inline ::IoTDNP::DatabaseNodeAddrStruct
makeAddrStruct(const QHostAddress& addr, const uint16_t port)
{
    const uint8_t* ipv6 = addr.toIPv6Address().c;
    const uint64_t ipv6Ls = *reinterpret_cast<const uint64_t*>(ipv6);
    const uint64_t ipv6Ms = *reinterpret_cast<const uint64_t*>(ipv6 + 8);
    return ::IoTDNP::DatabaseNodeAddrStruct {ipv6Ls, ipv6Ms, port};
}

inline void setAddrIpV6(uint8_t ipv6[], const uint64_t ipv6Ls, const uint64_t ipv6Ms)
{
    *reinterpret_cast<uint64_t*>(ipv6) = ipv6Ls;
    *reinterpret_cast<uint64_t*>(ipv6 + 8) = ipv6Ms;
}

template <typename Sender>
inline IConnectionInitiatorPtr makeAddConnection(Sender sender, TransactionId id,
                        JoinDatabaseNodeQueue::JoinDatabaseNodeControllerPtr trans)
{
    QObject::connect(sender, SIGNAL(addConnection(IConnectionInitiatorPtr)),
                     ConnectionContainer::globalInstance().getConnNotiffier(),
                     SLOT(addConnection(IConnectionInitiatorPtr)));

    auto addr = trans->addr();
    uint8_t addrIpV6 [16] {};
    setAddrIpV6(addrIpV6, addr.ipv6Ls(), addr.ipv6Ms());
    QHostAddress addrHost {addrIpV6};

    return std::make_shared<AddConnectionJoinDatabaseNodeInitiator>(
                addrHost, addr.port(), id);
}

template <typename Sender>
inline void popCommitTransaction(Sender* sender);

template <typename Sender>
inline void topReadyTransaction(Sender* sender)
{
    if(auto [id, trans] = JoinDatabaseNodeQueue::globalInstance().topReadyTransaction(); trans)
    {
        sender->disconnect();
        if(trans->isOwn())
        {
            ConnectionContainer::globalInstance().connectRecivers(
                        sender, SIGNAL(readyWrite(IMsgPtr)), trans->nodes());

            IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::StopJoinDatabaseNodeMsgPrototype>(
                        ReplicationServer::globalNodeId, makeTransactionIdStruct(id),
                        ::IoTDNP::CommitStatusEnum_COMMITED);

            sender->emitReadyWrite(msg);

            if(trans->setStopNode(ReplicationServer::globalNodeId,
                                  ::IoTDNP::CommitStatusEnum_COMMITED))
            {
                popCommitTransaction(sender);
            }
        }
        else
        {
            auto iconn = makeAddConnection(sender, id, trans);
            sender->emitAddConnection(iconn);
        }
    }
}

template <typename Sender>
inline void popCommitTransaction(Sender* sender)
{
    if(auto [id, trans, transList] = JoinDatabaseNodeQueue::globalInstance().popCommitTransaction(); trans)
    {
        sender->disconnect();
        ConnectionContainer::globalInstance().connectReciver(
                    sender, SIGNAL(readyWrite(IMsgPtr)), trans->joinNodeId());
        IMsgPtr msg {};
        if(trans->isOwn())
        {
            msg = makeIMsgPtr<Msg::IoTDNP::FinishJoinDatabaseNodeRespMsgPrototype>(
                        ConnectionContainer::globalInstance().transactionNodes());
            sender->emitReadyWrite(msg); // TODO: get status
        }
        if(trans->isCommited())
        {
            for(auto& [id, t] : transList)
            {
                if(t->isOwn())
                {
                    msg = makeIMsgPtr<Msg::IoTDNP::StartJoinDatabaseNodeMsgPrototype>(
                                ReplicationServer::globalNodeId, makeTransactionIdStruct(id),
                                t->joinNodeId(), &t->addr(), t->nodes());
                }
                else
                {
                    msg = makeIMsgPtr<Msg::IoTDNP::ReadyJoinDatabaseNodeMsgPrototype>(
                                ReplicationServer::globalNodeId, makeTransactionIdStruct(id),
                                ::IoTDNP::ReadyStatusEnum_PREPARED); //TDOD: get status
                }
                sender->emitReadyWrite(msg);
            }
        }
        topReadyTransaction(sender);
    }

}

} // namespace Handler
} // namespace ReplicationService

#endif // REPLICATIONSERVICE_HANDLER_JOINDATABASENODEHELPER_HPP
