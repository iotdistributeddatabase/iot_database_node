/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FetchDatabaseTableReqMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include <ReplicationServer.hpp>
#include <ConnectionContainer.hpp>
#include <DbConnectionPool.hpp>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QByteArray>
#include <QDataStream>

namespace ReplicationService
{
namespace Handler
{

FetchDatabaseTableReqMsgHandlerTask::FetchDatabaseTableReqMsgHandlerTask(
        MsgHandleType&& msg,
        const ReplicationConnectionManager *connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{
}

void FetchDatabaseTableReqMsgHandlerTask::run()
{
    auto db = Database::DbConnectionPool::globalInstance()->getConnection();
    QSqlQuery query {db};
    bool result = query.prepare("SELECT * FROM :table");
    query.bindValue(":table", msg_->table());
    if(result and not query.exec())
    {
        qCritical() << "FetchDatabaseTableReqMsgHandlerTask could not fetch table="
                    << msg_->table() << " db.lastError=" << db.lastError().text();
    }
    QSqlRecord r = query.record();
    std::vector<std::string> columnNames {};
    columnNames.reserve(static_cast<std::size_t>(r.count()));
    for (int i = 0; i < r.count(); ++i)
    {
        qDebug() << r.fieldName(i);
        columnNames.push_back(r.fieldName(i).toStdString());
    }
    QByteArray recordsData {};
    QDataStream recordsDataStream(&recordsData, QIODevice::WriteOnly);
    uint64_t rows {0};
    while(query.next())
    {
        for (int i = 0; i < r.count(); ++i)
        {
            recordsDataStream << query.value(i);
        }
        ++rows;
    }

    QObject::connect(this, &FetchDatabaseTableReqMsgHandlerTask::readyWrite,
                     connManager_, &ReplicationConnectionManager::doWrite);

    IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::FetchDatabaseTableRespMsgBuilder>(
                msg_->table()->string_view(), columnNames, rows,
                reinterpret_cast<const uint8_t*>(recordsData.data()),
                static_cast<std::size_t>(recordsData.size()));

    emit readyWrite(msg);
}

} // namespace Handler
} // namespace ReplicationService
