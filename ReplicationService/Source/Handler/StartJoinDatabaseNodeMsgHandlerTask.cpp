/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StartJoinDatabaseNodeMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include <cstdint>
#include <ReplicationServer.hpp>
#include <ConnectionNotifier.hpp>
#include <ConnectionContainer.hpp>
#include <TransactionId.hpp>
#include <JoinDatabaseNodeController.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include "JoinDatabaseNodeHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

StartJoinDatabaseNodeMsgHandlerTask::StartJoinDatabaseNodeMsgHandlerTask(
        MsgHandleType&& msg)
    : msg_(std::move(msg))
{
}

void StartJoinDatabaseNodeMsgHandlerTask::run()
{
    auto trans = JoinDatabaseNodeQueue::globalInstance().getOrAddIfNotExistTransaction(
                makeTransactionId(msg_->transactionId()));

    Nodes nodes = msg_->nodes() | ConnectionContainer::globalInstance().transactionNodes();

    bool result = trans->setTransaction(nodes, msg_->nodeId(),
                                        ReplicationServer::globalNodeId, msg_->joinNodeId(), msg_->addr());

    ConnectionContainer::globalInstance().connectRecivers(
                this, SIGNAL(readyWrite(IMsgPtr)), nodes);

    IMsgPtr msg =
            makeIMsgPtr<Msg::IoTDNP::ReadyJoinDatabaseNodeMsgPrototype>(
                ReplicationServer::globalNodeId, msg_->transactionId(),
                ::IoTDNP::ReadyStatusEnum_PREPARED);

    emit readyWrite(msg);

    if(result)
    {
        topReadyTransaction(this);
    }
    qInfo() << "handle StartJoinDatabaseNodeMsg with nodeId=" << msg_->nodeId()
            << " nodes=" << nodes << " msg.nodes=" << msg_->nodes();
}

void StartJoinDatabaseNodeMsgHandlerTask::emitReadyWrite(IMsgPtr msg)
{
    emit readyWrite(msg);
}

void StartJoinDatabaseNodeMsgHandlerTask::emitAddConnection(IConnectionInitiatorPtr init)
{
    emit addConnection(init);
}


} // namespace Handler
} // namespace ReplicationService
