/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_HANDLER_JOINDATABASENODEHANDLERTASK_HPP
#define REPLICATIONSERVICE_HANDLER_JOINDATABASENODEHANDLERTASK_HPP

#include <Util/Handler/HandlerTask.hpp>
#include <TransactionId.hpp>
#include <ReplicationConnectionManager.hpp>
#include <QObject>

namespace ReplicationService
{
namespace Handler
{

class JoinDatabaseNodeHandlerTask : public QObject,
                                    public Util::Handler::HandlerTask
{
    Q_OBJECT
public:
    explicit JoinDatabaseNodeHandlerTask(
            const ReplicationConnectionManager* connManager,
            const ConnectionState state);

    void run() override;

signals:
    void readyWrite(IMsgPtr msg);
    void deleteLaterObject();

public slots:

private:
    const ReplicationConnectionManager *connManager_;
    const ConnectionState state_;
};

} // namespace Handler
} // namespace ReplicationService

#endif // REPLICATIONSERVICE_HANDLER_JOINDATABASENODEHANDLERTASK_HPP
