/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConnectionSetupJoinDatabaseNodeHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>
#include <ReplicationServer.hpp>
#include <ConnectionContainer.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include <MessageBuffer.hpp>
#include <IoTDNP_generated.h>
#include "JoinDatabaseNodeHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

ConnectionSetupJoinDatabaseNodeHandlerTask
::ConnectionSetupJoinDatabaseNodeHandlerTask(const TransactionId transId,
                                             const ReplicationConnectionManager* connManager,
                                             const ConnectionState state)
    : transId_(transId), connManager_(connManager), state_(state)
{
}

void ConnectionSetupJoinDatabaseNodeHandlerTask::run()
{
    IMsgPtr msg {};
    auto trans = JoinDatabaseNodeQueue::globalInstance()
                    .getTransaction(transId_);
    if(state_ == ConnectionState::ConnectedState)
    {
        QObject::connect(this, SIGNAL(readyWrite(IMsgPtr)),
                         connManager_, SLOT(doWrite(IMsgPtr)), Qt::QueuedConnection);
        msg = makeIMsgPtr<Msg::IoTDNP::ConnJoinDatabaseNodeReqMsgPrototype>(
                    ReplicationServer::globalNodeId, makeTransactionIdStruct(transId_),
                    makeAddrStruct(ReplicationServer::globalAddress, ReplicationServer::globalPort));
        emit readyWrite(msg);
        qInfo() << "handle ConnectionSetupJoinDatabaseNode success node=" << ReplicationServer::globalNodeId;
    }
    else
    {
        QObject::connect(this, SIGNAL(deleteLaterObject()),
                         connManager_, SLOT(deleteLater()), Qt::QueuedConnection);
        emit deleteLaterObject();

        ConnectionContainer::globalInstance().connectRecivers(
                    this, SIGNAL(readyWrite(IMsgPtr)), trans->nodes());
        msg = makeIMsgPtr<Msg::IoTDNP::StopJoinDatabaseNodeMsgPrototype>(
                    ReplicationServer::globalNodeId, makeTransactionIdStruct(transId_),
                    ::IoTDNP::CommitStatusEnum_ABORTED);
        emit readyWrite(msg);

        if(trans->setStopNode(ReplicationServer::globalNodeId,
                              ::IoTDNP::CommitStatusEnum_ABORTED))
        {
            popCommitTransaction(this);
        }
        qWarning() << "handle ConnectionSetupJoinDatabaseNode fail with joinNodeId=" << trans->joinNodeId();
    }
}

void ConnectionSetupJoinDatabaseNodeHandlerTask::emitReadyWrite(IMsgPtr msg)
{
    emit readyWrite(msg);
}

void ConnectionSetupJoinDatabaseNodeHandlerTask::emitAddConnection(IConnectionInitiatorPtr init)
{
    emit addConnection(init);
}

} // namespace Handler
} // namespace ReplicationService
