/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_HANDLER_FETCHDATABASESTATEREQMSGHANDLERTASK_HPP
#define REPLICATIONSERVICE_HANDLER_FETCHDATABASESTATEREQMSGHANDLERTASK_HPP

#include <Util/Handler/HandlerTask.hpp>
#include <MessageHandle.hpp>
#include <MessageBuffer.hpp>
#include <IoTDNP_generated.h>
#include <ReplicationConnectionManager.hpp>
#include <QObject>

namespace ReplicationService
{
namespace Handler
{

class FetchDatabaseStateReqMsgHandlerTask : public QObject,
                                            public Util::Handler::HandlerTask
{
    Q_OBJECT
public:
    using MsgType = ::IoTDNP::FetchDatabaseStateReqMsg;
    using MsgHandleType = Msg::MessageHandle<MsgType>;
    explicit FetchDatabaseStateReqMsgHandlerTask(MsgHandleType&& msg,
                                                 const ReplicationConnectionManager* connManager);

    void run() override;

signals:
    void readyWrite(IMsgPtr msg);

public slots:

private:
    const MsgHandleType msg_;
    const ReplicationConnectionManager *connManager_;
};

} // namespace Handler
} // namespace ReplicationService

#endif // REPLICATIONSERVICE_HANDLER_FETCHDATABASESTATEREQMSGHANDLERTASK_HPP
