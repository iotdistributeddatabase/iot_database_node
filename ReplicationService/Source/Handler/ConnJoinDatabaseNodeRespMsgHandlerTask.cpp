/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConnJoinDatabaseNodeRespMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>
#include <ReplicationServer.hpp>
#include <ConnectionContainer.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include "JoinDatabaseNodeHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

ConnJoinDatabaseNodeRespMsgHandlerTask::ConnJoinDatabaseNodeRespMsgHandlerTask(
        MsgHandleType&& msg,
        const ReplicationConnectionManager *connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{
}

void ConnJoinDatabaseNodeRespMsgHandlerTask::run()
{
    auto trans = JoinDatabaseNodeQueue::globalInstance().getTransaction(
                makeTransactionId(msg_->transactionId()));

    const ConnectionInfo conn = makeConnectionInfo(connManager_, msg_->joinNodeId(),
                                                   &trans->addr());

    ConnectionContainer::globalInstance().addConnection(conn);

    ConnectionContainer::globalInstance().connectRecivers(
                this, SIGNAL(readyWrite(IMsgPtr)), trans->nodes());

    IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::StopJoinDatabaseNodeMsgPrototype>(
                ReplicationServer::globalNodeId, msg_->transactionId(),
                ::IoTDNP::CommitStatusEnum_COMMITED);

    emit readyWrite(msg);

    if(trans->setStopNode(ReplicationServer::globalNodeId,
                          ::IoTDNP::CommitStatusEnum_COMMITED))
    {
        popCommitTransaction(this);
    }
    qInfo() << "handle ConnJoinDatabaseNodeRespMsg with nodeId=" << msg_->joinNodeId();
}

void ConnJoinDatabaseNodeRespMsgHandlerTask::emitReadyWrite(IMsgPtr msg)
{
    emit readyWrite(msg);
}

void ConnJoinDatabaseNodeRespMsgHandlerTask::emitAddConnection(IConnectionInitiatorPtr init)
{
    emit addConnection(init);
}

} // namespace Handler
} // namespace ReplicationService
