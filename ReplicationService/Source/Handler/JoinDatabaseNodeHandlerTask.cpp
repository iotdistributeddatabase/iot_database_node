/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "JoinDatabaseNodeHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include "JoinDatabaseNodeHelper.hpp"
#include <ReplicationServer.hpp>

namespace ReplicationService
{
namespace Handler
{

JoinDatabaseNodeHandlerTask
::JoinDatabaseNodeHandlerTask(
        const ReplicationConnectionManager* connManager,
        const ConnectionState state)
    : connManager_(connManager), state_(state)
{
}

void JoinDatabaseNodeHandlerTask::run()
{
    if(state_ == ConnectionState::ConnectedState)
    {
        QObject::connect(this, SIGNAL(readyWrite(IMsgPtr)),
                         connManager_, SLOT(doWrite(IMsgPtr)),
                         Qt::QueuedConnection);
        IMsgPtr msg
                = makeIMsgPtr<Msg::IoTDNP::InitJoinDatabaseNodeReqMsgPrototype>(
                    ReplicationServer::globalNodeId,
                    makeAddrStruct(ReplicationServer::globalAddress,
                                   ReplicationServer::globalPort));
        emit readyWrite(msg);
    }
    else
    {
        QObject::connect(this, SIGNAL(deleteLaterObject()),
                         connManager_, SLOT(deleteLater()),
                         Qt::QueuedConnection);
        emit deleteLaterObject();
        qFatal("ReplicationService could not join to the database nodes");
    }
}

} // namespace Handler
} // namespace ReplicationService
