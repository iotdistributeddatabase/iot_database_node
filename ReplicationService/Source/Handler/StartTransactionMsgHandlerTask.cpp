/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "StartTransactionMsgHandlerTask.hpp"
#include "DatabaseTransactionHelper.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include <ReplicationServer.hpp>
#include <TableContainer.hpp>
#include <ConnectionContainer.hpp>
#include <QDebug>

namespace ReplicationService
{
namespace Handler
{

StartTransactionMsgHandlerTask::StartTransactionMsgHandlerTask(MsgHandleType&& msg)
    : msg_(std::move(msg))
{

}

void StartTransactionMsgHandlerTask::run()
{
    const char* tableName = msg_->table()->c_str();
    auto table = TableContainer::globalInstance()
            .getOrIfNotExistAddTable(tableName);

    TransactionId transId = makeTransactionId(msg_->transactionId());

    auto transaction = table->getOrAddIfNotExistTransaction(transId);

    ConnectionContainer::globalInstance().connectRecivers(
                this, SIGNAL(readyWrite(IMsgPtr)), msg_->nodes());

    IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::ReadyTransactionMsgPrototype>(
                tableName, msg_->transactionId(),
                ReplicationServer::globalNodeId,
                ::IoTDNP::ReadyStatusEnum_PREPARED);

    emit readyWrite(msg);

    bool isReady = transaction->setTransaction(
                msg_->nodes(),
                msg_->nodeId(),
                msg_->cmd()->c_str(),
                ReplicationServer::globalNodeId);

//    qInfo() << "StartTransactionMsgHandlerTask: nodes="
//             << msg_->nodes() << " isRead=" << isReady
//             << " nodeId=" << msg_->nodeId()
//             << " transId=" << transId << " tableName=" << tableName
//             << " tablePtr=" << static_cast<void*>(table.get())
//             << " transPtr=" << static_cast<void*>(transaction.get());

    if(isReady)
    {
        topReadyTransaction(this, table, tableName);
    }
}

} // namespace ReplicationService
} // namespace Handler
