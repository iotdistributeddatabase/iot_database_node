/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "InitJoinDatabaseNodeReqMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include <cstdint>
#include <ReplicationServer.hpp>
#include <ConnectionContainer.hpp>
#include <TransactionId.hpp>
#include <JoinDatabaseNodeController.hpp>
#include <JoinDatabaseNodeQueue.hpp>
#include "JoinDatabaseNodeHelper.hpp"

namespace ReplicationService
{
namespace Handler
{

InitJoinDatabaseNodeReqMsgHandlerTask::InitJoinDatabaseNodeReqMsgHandlerTask(
        MsgHandleType&& msg,
        const ReplicationConnectionManager* connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{
}

void InitJoinDatabaseNodeReqMsgHandlerTask::run()
{
    const ConnectionInfo conn = makeConnectionInfo(connManager_, msg_->joinNodeId(),
                                                   msg_->addr());

    ConnectionContainer::globalInstance().addConnection(conn);

    TransactionId transId = makeTransactionId();
    auto trans = JoinDatabaseNodeQueue::makeJoinDatabaseNodeController(
                     ReplicationServer::globalNodeId,
                     msg_->joinNodeId(), msg_->addr());

    auto [r, nodes] = JoinDatabaseNodeQueue::globalInstance().addTransaction(transId, trans);
    if(not r)
    {
        qCritical() << "could not add JoinDatabaseNode transaction";
        return;
    }
    QObject::connect(this, SIGNAL(readyWriteResp(IMsgPtr)),
                     connManager_, SLOT(doWrite(IMsgPtr)),
                     Qt::QueuedConnection);
    IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::InitJoinDatabaseNodeRespMsgPrototype>(
                ReplicationServer::globalNodeId,
                makeAddrStruct(ReplicationServer::globalAddress, ReplicationServer::globalPort));
    emit readyWriteResp(msg);

    if(nodes != ReplicationServer::globalNodeId)
    {
        ConnectionContainer::globalInstance().connectRecivers(
                    this, SIGNAL(readyWrite(IMsgPtr)), nodes);

        IMsgPtr msg =
                makeIMsgPtr<Msg::IoTDNP::StartJoinDatabaseNodeMsgPrototype>(
                    ReplicationServer::globalNodeId, makeTransactionIdStruct(transId),
                    msg_->joinNodeId(), msg_->addr(), nodes);

        emit readyWrite(msg);
        qInfo() << "handle InitJoinDatabaseNodeReqMsg emit StartJoinDatabaseNodeMsg";
    }
    else
    {
        topReadyTransaction(this);
    }
    qInfo() << "handle InitJoinDatabaseNodeReqMsg with joinNodeId=" << msg_->joinNodeId()
            << " nodes" << nodes;
}

void InitJoinDatabaseNodeReqMsgHandlerTask::emitReadyWrite(IMsgPtr msg)
{
    emit readyWrite(msg);
}

void InitJoinDatabaseNodeReqMsgHandlerTask::emitAddConnection(IConnectionInitiatorPtr init)
{
    emit addConnection(init);
}

} // namespace Handler
} // namespace ReplicationService
