/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FetchDatabaseStateReqMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>
#include <ReplicationServer.hpp>
#include <ConnectionContainer.hpp>
#include <DatabaseTableStates.hpp>

namespace ReplicationService
{
namespace Handler
{

FetchDatabaseStateReqMsgHandlerTask::FetchDatabaseStateReqMsgHandlerTask(
        MsgHandleType&& msg,
        const ReplicationConnectionManager *connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{
}

void FetchDatabaseStateReqMsgHandlerTask::run()
{
    QObject::connect(this, &FetchDatabaseStateReqMsgHandlerTask::readyWrite,
                     connManager_, &ReplicationConnectionManager::doWrite);

    IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::FetchDatabaseStateRespMsgBuilder>();

    Msg::IoTDNP::FetchDatabaseStateRespMsgBuilder* fetchMsg
            = dynamic_cast<Msg::IoTDNP::FetchDatabaseStateRespMsgBuilder*>(msg.get());
    auto transId = DatabaseTableStates::globalInstance().saveTableStatesToMsg(fetchMsg);
    fetchMsg->finish(transId);

    emit readyWrite(msg);
}

} // namespace Handler
} // namespace ReplicationService
