/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "FetchDatabaseStateRespMsgHandlerTask.hpp"
#include <IoTDNPMsgPrototypes.hpp>

#include <ReplicationServer.hpp>
#include <DatabaseTableStates.hpp>
#include <DbConnectionPool.hpp>
#include <QtSql/QSqlError>

namespace ReplicationService
{
namespace Handler
{

FetchDatabaseStateRespMsgHandlerTask::FetchDatabaseStateRespMsgHandlerTask(
        Msg::MessageBuffer &&msgBuf,
        const IoTDNP::FetchDatabaseStateRespMsg* msg,
        const ReplicationConnectionManager *connManager)
    : msgBuff_(std::move(msgBuf)), msg_(msg), connManager_(connManager)
{
}

void FetchDatabaseStateRespMsgHandlerTask::run()
{
    for(auto it = msg_->tableStates()->cbegin(); it != msg_->tableStates()->cend(); ++it)
    {
        if(not DatabaseTableStates::globalInstance().insertTableState(it->table()->str(),
                                                               it->tableTransId()))
        {
            qCritical() << "FetchDatabaseStateRespMsgHandlerTask could not insert table="
                        << it->table()->c_str();
        }
    }
    // Fetch Database Tables
    QObject::connect(this, &FetchDatabaseStateRespMsgHandlerTask::readyWrite,
                     connManager_, &ReplicationConnectionManager::doWrite);
    auto db = Database::DbConnectionPool::globalInstance()->getConnection();
    auto tables = db.tables();
    if(tables.empty())
    {
        qCritical() << "FetchDatabaseStateRespMsgHandlerTask could not read tables error="
                    << db.lastError().text();
    }
    for (auto it = tables.constBegin(); it != tables.constEnd(); ++it)
    {
        IMsgPtr msg = makeIMsgPtr<Msg::IoTDNP::FetchDatabaseTableReqMsgBuilder>(
                           it->toLocal8Bit().constData());

        emit readyWrite(msg);
    }
}

} // namespace Handler
} // namespace ReplicationService
