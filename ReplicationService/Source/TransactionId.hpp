/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_TRANSACTIONID_HPP
#define REPLICATIONSERVICE_TRANSACTIONID_HPP

#include <cstdint>
#include <functional>
#include <QDebug>
#include <IoTDNP_generated.h>

namespace ReplicationService
{

struct TransactionId
{
    int64_t timeMsSinceEpoch;
    uint64_t nodeId;
    uint64_t nodeCounter;
};

static constexpr auto nullTransactionId = TransactionId{0, 0, 0};

bool operator==(const TransactionId& lhs, const TransactionId& rhs) noexcept;

struct CompareTransactionId
{
    bool operator()(const TransactionId& a, const TransactionId& b) const noexcept
    {
        return a.timeMsSinceEpoch == b.timeMsSinceEpoch
                ? (a.nodeId == b.nodeId ? a.nodeCounter < b.nodeCounter : a.nodeId < b.nodeId)
                : a.timeMsSinceEpoch < b.timeMsSinceEpoch;
    }
};

struct TransactionIdHash
{
    std::size_t operator()(const TransactionId& t) const noexcept
    {
        std::size_t h1 = std::hash<int64_t>{}(t.timeMsSinceEpoch);
        std::size_t h2 = std::hash<uint64_t>{}(t.nodeId);
        std::size_t h3 = std::hash<uint64_t>{}(t.nodeCounter);
        return (h1 ^ (h2 << 1)) ^ (h3 << 2);
    }
};

TransactionId makeTransactionId();
TransactionId makeTransactionId(const IoTDNP::TransactionIdStruct& id);
TransactionId makeTransactionId(const IoTDNP::TransactionIdStruct* id);
TransactionId makeTransactionId(const int64_t timeMsSinceEpoch, const uint64_t nodeId, const uint64_t nodeCunter);
IoTDNP::TransactionIdStruct makeTransactionIdStruct(const TransactionId& id);

QDebug operator<< (QDebug d, const TransactionId& id);

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_TRANSACTIONID_HPP
