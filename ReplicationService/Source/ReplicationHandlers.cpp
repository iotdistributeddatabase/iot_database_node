/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ReplicationHandlers.hpp>
#include "TableContainer.hpp"
#include <TableController.hpp>
#include <TransactionController.hpp>
#include <ConnectionContainer.hpp>
#include <ReplicationServer.hpp>
#include "SenderMsgSignal.hpp"
#include <MessageTypes.hpp>
#include <IoTDNPMsgPrototypes.hpp>

#include <Util/UniquePtrWithDeleteLater.hpp>
#include "Handler/ReadyTransactionReplicationHandlerTask.hpp"
#include <QThreadPool>

namespace ReplicationService
{

bool createDatabaseTransaction(const char* table, const char* cmd,
                              const HandleTransactionFinish handleFinish)
{
    // TODO check if the replication is finish
    auto transId = makeTransactionId();
    auto transIdStruct = makeTransactionIdStruct(transId);
    auto transNodes = ConnectionContainer::globalInstance().transactionNodes();
    auto trans = TableController::makeTransactionController(transNodes, ReplicationServer::globalNodeId,
                                                            cmd, handleFinish);
    auto tableController = TableContainer::globalInstance()
            .getOrIfNotExistAddTable(table);
    tableController->addTransaction(transId, trans);

    auto sender = Util::makeUniquePtrWithDeleteLater<SenderMsgSignal>();

    ConnectionContainer::globalInstance()
            .connectRecivers(sender.get(), SIGNAL(readyWrite(IMsgPtr)),
                             transNodes);

    auto msg = makeIMsgPtr<Msg::IoTDNP::StartTransactionMsgPrototype>(
                table, &transIdStruct, ReplicationServer::globalNodeId,
                transNodes, cmd);

    emit sender->readyWrite(msg);

    if(transNodes == ReplicationServer::globalNodeId)
    {
        qDebug() << "startDatabaseTransaction::start";
        QThreadPool::globalInstance()->start(new Handler::ReadyTransactionReplicationHandlerTask(table));
    }

    return true;
}

} // namespace ReplicationService
