/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_TABLETableTransactionController_HPP
#define REPLICATIONSERVICE_TABLETableTransactionController_HPP

#include "TransactionController.hpp"
#include "ReplicationHandlers.hpp"
#include "ReplicationServiceTypes.hpp"
#include <cstdint>
#include <string>
#include <mutex>
#include <IoTDNP_generated.h>

namespace ReplicationService
{

class TableTransactionController : public TransactionController
{
public:
    TableTransactionController(const TableTransactionController&) = delete;
    TableTransactionController& operator=(const TableTransactionController&) = delete;
    TableTransactionController(TableTransactionController&&) = delete;
    TableTransactionController& operator=(TableTransactionController&&) = delete;
    explicit TableTransactionController();
    explicit TableTransactionController(const Nodes nodes, const NodeId ownNodeId,
                                        const char* const cmd,
                                        const HandleTransactionFinish handle);

    bool setTransaction(const Nodes nodes, const NodeId ownNodeId,
                        const char * const cmd, const NodeId readyNode);

    ::IoTDNP::CommitStatusEnum readyTransaction();
    bool commitTransaction();
    void ifPresentHandleTransFinish(bool result, const char* error);

private:
    std::string cmd_;
    HandleTransactionFinish handleTransFinish_;
};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_TABLETRANSACTIONCONTROLLER_HPP
