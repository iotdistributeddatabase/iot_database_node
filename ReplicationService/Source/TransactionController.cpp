/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TransactionController.hpp"
#include <QtSql/QSqlQuery>
#include <DbConnectionPool.hpp>
#include <QDebug>
#include <QtSql/QSqlError>

namespace ReplicationService
{

using namespace StateMachine;

TransactionController::TransactionController()
    : mutex_(), isOwn_{false}
{
}

TransactionController::TransactionController(const Nodes nodes,
                                             const NodeId ownNodeId)
    : mutex_(), isOwn_{true}, smState_{nodes, 0}
{
    sm_.process_event(ReadyNode{ownNodeId});
}

void TransactionController::updateNodes(const NodeId node)
{
    std::lock_guard guard{mutex_};
    smState_.nodes |= node;
    sm_.process_event(UpdateNodes{});
}

bool TransactionController::setTransaction(const Nodes nodes,
                                           const NodeId ownNodeId,
                                           const NodeId readyNode)
{
    std::lock_guard<std::mutex> guard {mutex_};
    smState_.nodes = nodes;
    sm_.process_event(ReadyNode{ownNodeId | readyNode});
    return isReadyEnded_();
}

bool TransactionController::setReadyNode(const NodeId node,
                                         const ::IoTDNP::ReadyStatusEnum status)
{
    std::lock_guard guard{mutex_};
    switch (status)
    {
    case ::IoTDNP::ReadyStatusEnum_PREPARED:
        sm_.process_event(ReadyNode{node});
        break;
    case ::IoTDNP::ReadyStatusEnum_ABORTED:
        sm_.process_event(AbortNode{node});
        sm_.process_event(Finish{});
        break;
    case ::IoTDNP::ReadyStatusEnum_CRASHED:
    case ::IoTDNP::ReadyStatusEnum_EMPTY:
    case ::IoTDNP::ReadyStatusEnum_INVALID:
    case ::IoTDNP::ReadyStatusEnum_RFU:
        sm_.process_event(CrashNode{node});
    }
    return isReadyEnded_();
}

bool TransactionController::setStopNode(const NodeId node,
                                        const ::IoTDNP::CommitStatusEnum status)
{
    std::lock_guard guard{mutex_};
    switch (status)
    {
    case ::IoTDNP::CommitStatusEnum_COMMITED:
        sm_.process_event(StopNode{node});
        break;
    case ::IoTDNP::CommitStatusEnum_ABORTED:
        sm_.process_event(AbortNode{node});
        sm_.process_event(Finish{});
        break;
    case ::IoTDNP::CommitStatusEnum_CRASHED:
    case ::IoTDNP::CommitStatusEnum_EMPTY:
    case ::IoTDNP::CommitStatusEnum_INVALID:
    case ::IoTDNP::CommitStatusEnum_RFU:
        sm_.process_event(CrashNode{node});
    }
    return isCommitEnded_();
}

bool TransactionController::isReadyTransaction()
{
    std::lock_guard guard{mutex_};
    if(isReadyEnded_())
    {
        sm_.process_event(Finish{});
        return true;
    }
    return  false;
}

bool TransactionController::isReadied() const
{
    return sm_.is(TransactionSM::Readied);
}

bool TransactionController::isCommitTransaction()
{
    std::lock_guard guard{mutex_};
    if(isCommitEnded_())
    {
        sm_.process_event(Finish{});
        return true;
    }
    return false;
}

bool TransactionController::isCommited() const
{
    return sm_.is(TransactionSM::Commited);
}

bool TransactionController::isOwn() const
{
    return isOwn_;
}

Nodes TransactionController::nodes() const
{
    return smState_.nodes;
}

bool TransactionController::isReadyEnded_() const
{
    return sm_.is(TransactionSM::Ready) or sm_.is(TransactionSM::NotReadied);
}

bool TransactionController::isCommitEnded_() const
{
    return sm_.is(TransactionSM::Commit) or sm_.is(TransactionSM::NotCommited);
}

} // namespace ReplicationService
