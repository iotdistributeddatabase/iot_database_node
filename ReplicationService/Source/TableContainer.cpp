/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TableContainer.hpp"
#include <QDebug>

namespace ReplicationService
{

TableContainer::TableControllerPtr TableContainer::getTable(const TableNameStrKey& name)
{
    std::lock_guard<std::mutex> guard{mutex_};
    return tables_[name];
}

TableContainer::TableControllerPtr TableContainer::getOrIfNotExistAddTable(const TableNameStrKey& name)
{
    std::lock_guard<std::mutex> guard{mutex_};
    TableControllerPtr p {nullptr};
    auto search = tables_.find(name);
    if(search == tables_.end())
    {
        p = std::make_shared<TableController>();
        auto result = tables_.insert({name, p});
        if(not result.second)
        {
            p.reset();
            qWarning() << "Could not add new table " << QString::fromUtf8(name.data(),
                                                                          static_cast<int>(name.size()));
        }
    }
    else {
        p = search->second;
    }
    return p;
}

TableContainer &TableContainer::globalInstance()
{
    static TableContainer container;
    return container;
}

} // namespace ReplicationService
