/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TableController.hpp"

namespace ReplicationService
{

bool TableController::addTransaction(TransactionId id, TableController::TransactionControllerPtr t)
{
    std::lock_guard<std::mutex> guard{mutex_};
    auto result = transactions_.insert({id, t});
    if(not result.second)
    {
        qWarning() << "addTransaction: Could not add new " << id;
    }
    return result.second;
}

TableController::TransactionControllerPtr TableController::getTransaction(TransactionId id)
{
    std::lock_guard<std::mutex> guard{mutex_};
    return transactions_[id];
}

TableController::TransactionControllerPtr TableController::getOrAddIfNotExistTransaction(TransactionId id)
{
    std::lock_guard<std::mutex> guard{mutex_};
    TransactionControllerPtr p {nullptr};
    auto search = transactions_.find(id);
    if(search == transactions_.end())
    {
        p = makeTransactionController();
        auto result = transactions_.insert({id, p});
        if(not result.second)
        {
            p.reset();
            qWarning() << "getOrAddIfNotExistTransaction: Could not add new " << id;
        }
    }
    else
    {
        p = search->second;
    }
    return p;
}

void TableController::removeTransaction(TransactionId id)
{
    std::lock_guard<std::mutex> guard{mutex_};
    transactions_.erase(id);
}

TableController::TransactionPair TableController::topReadyTransaction()
{
    std::lock_guard<std::mutex> guard{mutex_};
    if(not transactions_.empty())
    {
        auto b = transactions_.begin();
        if(b->second->isReadyTransaction())
        {
            return *b;
        }
    }
    return {nullTransactionId, nullptr};
}

TableController::TransactionPair TableController::popCommitTransaction()
{
    std::lock_guard<std::mutex> guard{mutex_};
    if(not transactions_.empty())
    {
        auto b = transactions_.begin();
        TransactionPair p {*b};
        if(p.second->isCommitTransaction())
        {
            transactions_.erase(b);
            return p;
        }
    }
    return {nullTransactionId, nullptr};
}

void TableController::popTransaction()
{
    std::lock_guard<std::mutex> guard{mutex_};
    if(not transactions_.empty())
    {
        transactions_.erase(transactions_.begin());
    }
}

} // namespace ReplicationService
