/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ReplicationConnectionManager.hpp"
#include "MessageDispatcher.hpp"

namespace ReplicationService
{

ReplicationConnectionManager::ReplicationConnectionManager(
        const QHostAddress& addr, const quint16 port)
    : socketDescriptor_(-1), addr_(addr), port_(port)
{

}

ReplicationConnectionManager::ReplicationConnectionManager(
        const qintptr socketDescriptor)
    : socketDescriptor_(socketDescriptor), addr_("::1"), port_(0)
{

}

ReplicationConnectionManager::~ReplicationConnectionManager()
{
    emit deleted(this);
}

void ReplicationConnectionManager::doWork()
{
    if((socket_ = std::make_unique<QTcpSocket>()))
    {
        if(socketDescriptor_ > -1)
        {
            if(not socket_->setSocketDescriptor(socketDescriptor_))
            {
                socket_.reset();
                qCritical() << "Replication Connection could not set socketDescriptor ="
                            << socketDescriptor_;
            }
        }
        else
        {
            socket_->connectToHost(addr_, port_);
        }
    }
    else
    {
        qCritical() << "Replication Connection could not allocate socketDescriptor ="
                    << socketDescriptor_ << " addr=" << addr_ << " port=" << port_;
    }
    if(socket_)
    {
        auto handler = std::bind(&ReplicationConnectionManager::receiveMsg,
                                 this, std::placeholders::_1);
        sink_ = std::make_unique<Msg::MessageSink>(*socket_, handler);
        connect(socket_.get(), SIGNAL(connected()),
                this, SLOT(onConnected()));
        connect(socket_.get(), SIGNAL(disconnected()),
                this, SLOT(onDisconnect()));
        connect(socket_.get(), SIGNAL(error(QAbstractSocket::SocketError)),
                this, SLOT(error(QAbstractSocket::SocketError)));
        connect(socket_.get(), SIGNAL(readyRead()),
                this, SLOT(onReadyRead()));
    }
    else
    {
        qCritical() << "Replication Connection could not connect socketDescriptor ="
                    << socketDescriptor_ << " addr=" << addr_ << " port=" << port_;
        quit();
    }
}

void ReplicationConnectionManager::onConnected()
{
    emit establishConnection(this, static_cast<ConnectionState>(socket_->state()));
    qDebug() << "Replication connected socketDescriptor =" << socketDescriptor_;
}

void ReplicationConnectionManager::onDisconnect()
{
    qDebug() << "Replication disconnected socketDescriptor =" << socketDescriptor_;
    quit();
}

void ReplicationConnectionManager::error(QAbstractSocket::SocketError socketError)
{
    emit establishConnection(this, static_cast<ConnectionState>(socket_->state()));
    qDebug() << "Replication error socketDescriptor =" << socketDescriptor_
             << "socketError=" << socketError;
}

void ReplicationConnectionManager::onReadyRead()
{
    qDebug() << "Replication onReadyRead recived bytes =" << socket_->bytesAvailable()
             << "socketDescriptor =" << socketDescriptor_;
    sink_->receive();
}

void ReplicationConnectionManager::receiveMsg(Msg::MessageBuffer&& msgBuf)
{
    MessageDispatcher::dispatch(std::move(msgBuf), this);
}

void ReplicationConnectionManager::doWrite(IMsgPtr msg)
{
    if(sink_)
    {
        sink_->send(*msg);
    }
}


void ReplicationConnectionManager::quit()
{
    emit finished(this);
}

} // namespace ReplicationService
