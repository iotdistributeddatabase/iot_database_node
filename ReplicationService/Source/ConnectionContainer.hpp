/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef CONNECTIONCONTAINER_HPP
#define CONNECTIONCONTAINER_HPP

#include "ConnectionInfo.hpp"
#include <vector>
#include <mutex>
#include <QDebug>
#include <atomic>

namespace ReplicationService
{

class ConnectionNotifier;

class ConnectionContainer
{
public:
    ConnectionContainer() = delete;
    ConnectionContainer(const ConnectionContainer&) = delete;
    ConnectionContainer& operator=(const ConnectionContainer&) = delete;
    ConnectionContainer(ConnectionContainer&&) = delete;
    ConnectionContainer& operator=(ConnectionContainer&&) = delete;
    explicit ConnectionContainer(const std::size_t size);

    static ConnectionContainer& globalInstance();

    void addConnection(const ConnectionInfo& conn);
    void removeConnection(const ReplicationConnectionManager *conn);
    template<typename Sender, typename Signal>
    void connectRecivers(const Sender* sender, const Signal signal, const Nodes nodes)
    {
        std::lock_guard guard{mutex_};
        for(const auto& conn : conns_)
        {
            if(conn.nodeId & nodes)
            {
                qDebug() << "ConnectionContainer::connectRecivers"
                         << "conn=" << static_cast<const void*>(conn.connManager)
                         << "nodeId=" << conn.nodeId
                         << "sender=" << static_cast<const void*>(sender);
                QObject::connect(sender, signal,
                                 conn.connManager, SLOT(doWrite(IMsgPtr)),
                                 Qt::QueuedConnection);
            }
        }
    }
    template<typename Sender, typename Signal>
    void connectReciver(const Sender* sender, const Signal signal, const NodeId nodeId)
    {
        std::lock_guard guard{mutex_};
        auto search = std::find_if(conns_.begin(), conns_.end(),
                                   [nodeId](const ConnectionInfo& conn){ return conn.nodeId == nodeId; });
        if(search != conns_.end())
        {
            qDebug() << "ConnectionContainer::connectReciver"
                     << "conn=" << search->connManager
                     << "nodeId=" << search->nodeId
                     << "sender=" << sender;
            QObject::connect(sender, signal,
                             search->connManager, SLOT(doWrite(IMsgPtr)),
                             Qt::QueuedConnection);
        }
        else
        {
            qWarning() << "ConnectionContainer::connectReciver could not connect nodeId=" <<nodeId;
        }
    }
    Nodes transactionNodes();
    void addTransactionNode(const NodeId nodeId);
    const ConnectionNotifier *getConnNotiffier() const;
    void setConnNotiffier(const ConnectionNotifier *value);

private:
    std::mutex mutex_;
    std::vector<ConnectionInfo> conns_;
    Nodes transactionNodes_;
    std::atomic<const ConnectionNotifier*> connNotiffier_;
};

} // namespace ReplicationService

#endif // CONNECTIONCONTAINER_HPP
