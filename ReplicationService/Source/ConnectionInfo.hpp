/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_CONNECTIONINFO_HPP
#define REPLICATIONSERVICE_CONNECTIONINFO_HPP

#include "ReplicationConnectionManager.hpp"
#include "ReplicationServiceTypes.hpp"
#include <IoTDNP_generated.h>

namespace ReplicationService
{

struct ConnectionInfo
{
    const ReplicationConnectionManager* connManager;
    NodeId nodeId;
    IpV6AddrLs ipv6Ls;
    IpV6AddrMs ipv6Ms;
    Port port;
};

ConnectionInfo makeConnectionInfo(const ReplicationConnectionManager* connManager,
                                  const NodeId nodeId,
                                  const ::IoTDNP::DatabaseNodeAddrStruct* addr);

} // ReplicationService

#endif // REPLICATIONSERVICE_CONNECTIONINFO_HPP
