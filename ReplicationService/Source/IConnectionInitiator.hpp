/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_ICONNECTIONINITIATOR_HPP
#define REPLICATIONSERVICE_ICONNECTIONINITIATOR_HPP

#include "ReplicationConnectionManager.hpp"
#include <QHostAddress>
#include <QAbstractSocket>
#include <QMetaType>

namespace ReplicationService
{

class IConnectionInitiator
{
public:
    explicit IConnectionInitiator(const QHostAddress& addr, const quint16 port);
    virtual ~IConnectionInitiator();

    virtual void handleConnectionSetup(const ReplicationConnectionManager* conn,
                                       const ConnectionState state) = 0;

    QHostAddress addr() const;
    quint16 port() const;

protected:
    QHostAddress addr_;
    quint16 port_;
};

} // namespace ReplicationService

typedef std::shared_ptr< ReplicationService::IConnectionInitiator > IConnectionInitiatorPtr;
Q_DECLARE_METATYPE( IConnectionInitiatorPtr )
const int typeIdIConnectionInitiatorPtr = qRegisterMetaType< IConnectionInitiatorPtr >( "IConnectionInitiatorPtr" );

#endif // REPLICATIONSERVICE_ICONNECTIONINITIATOR_HPP
