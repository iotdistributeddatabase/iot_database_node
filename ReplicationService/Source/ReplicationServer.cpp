/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <ReplicationServer.hpp>
#include "ReplicationConnectionManager.hpp"
#include "ConnectionNotifier.hpp"
#include "ConnectionContainer.hpp"
#include "JoinDatabaseNodeInitiator.hpp"
#include <memory>
#include <QObject>
#include <functional>

namespace ReplicationService
{

ReplicationServer::NodeId ReplicationServer::globalNodeId {};
QHostAddress ReplicationServer::globalAddress {};
quint16 ReplicationServer::globalPort {};

using std::placeholders::_1;
using std::placeholders::_2;

ReplicationServer::ReplicationServer(const QHostAddress &address,
                                     const quint16 port,
                                     const std::size_t max_threads,
                                     const NodeId nodeId)
    : address_(address), port_(port),
      connNotifier_(std::make_unique<ConnectionNotifier>(
                        std::bind(&ReplicationServer::connectToDatabaseNode, this, _1, _2))),
      workerPool_(max_threads)
{
    globalNodeId = 1 << (nodeId - 1);
    globalAddress = address;
    globalPort = port;
    ConnectionContainer::globalInstance().setConnNotiffier(connNotifier_.get());
}

ReplicationServer::~ReplicationServer()
{
    ConnectionContainer::globalInstance().setConnNotiffier(nullptr);
}

bool ReplicationServer::startServer()
{
    if(this->listen(address_, port_))
    {
        qInfo() << "Replication Server is listening on address"
                << address_ << "and port " << port_ << "...";
        return true;
    }
    else
    {
        qCritical() << "Replication Server could not start on address ="
                    << address_ << "and port =" << port_;
    }
    return false;
}

bool ReplicationServer::joinDatabaseNode(const QHostAddress &address, const quint16 port)
{
    qInfo() << "Replication Server is joining the address"
            << address << "and the port " << port << "...";
    IConnectionInitiatorPtr connJoin
            = std::make_shared<JoinDatabaseNodeInitiator>(address, port);
    connNotifier_->addConnection(connJoin);
    return true;
}

bool ReplicationServer::stopServer()
{
    this->close();
    return true;
}

namespace
{
inline void connectNotifier(ReplicationConnectionManager* conn, ConnectionNotifier* notifier)
{
    QObject::connect(conn, SIGNAL(deleted(const ReplicationConnectionManager*)),
                     notifier, SLOT(destroyedConnection(const ReplicationConnectionManager*)),
                     Qt::QueuedConnection);
}
}

QObject* ReplicationServer::connectToDatabaseNode(const QHostAddress& address,
                                                  const quint16 port)
{
    std::unique_ptr<ReplicationConnectionManager> worker
            = std::make_unique<ReplicationConnectionManager>(address, port);
    QObject* ret = worker.get();
    connectNotifier(worker.get(), connNotifier_.get());
    QObject::connect(worker.get(), SIGNAL(establishConnection(const ReplicationConnectionManager*,
                                               const ConnectionState)),
                     connNotifier_.get(), SLOT(establishConnection(const ReplicationConnectionManager*,
                                                           const ConnectionState)));
    workerPool_.addWorker(std::move(worker));
    return ret;
}

void ReplicationServer::incomingConnection(qintptr socketDescriptor)
{
    qDebug() << "Replication incoming connection socketDescriptor =" << socketDescriptor;

    std::unique_ptr<ReplicationConnectionManager> worker
            = std::make_unique<ReplicationConnectionManager>(socketDescriptor);
    connectNotifier(worker.get(), connNotifier_.get());
    workerPool_.addWorker(std::move(worker));
}


} // namespace ReplicationService
