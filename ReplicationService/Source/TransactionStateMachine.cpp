#include "TransactionStateMachine.hpp"

QDebug operator<< (QDebug d, const ReplicationService::StateMachine::TransactionState& s)
{
    d << "TransactionState{nodes=" << s.nodes
      << ", receivedNodes=" << s.receivedNodes << "}";
    return d;
}
