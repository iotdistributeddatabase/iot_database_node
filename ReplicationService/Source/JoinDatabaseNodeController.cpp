/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "JoinDatabaseNodeController.hpp"
#include "ConnectionContainer.hpp"

namespace ReplicationService
{

using namespace StateMachine;

JoinDatabaseNodeController::JoinDatabaseNodeController()
    : TransactionController(), joinNodeId_(0), addr_()
{

}

JoinDatabaseNodeController::JoinDatabaseNodeController(const NodeId ownNodeId,
                                                       const NodeId joinNodeId,
                                                       const ::IoTDNP::DatabaseNodeAddrStruct* addr)
    : TransactionController(0, ownNodeId), joinNodeId_(joinNodeId), addr_(*addr)
{
}

bool JoinDatabaseNodeController::setTransaction(const Nodes nodes,
                                                const NodeId readyNode,
                                                const NodeId ownNodeId,
                                                const NodeId joinNodeId,
                                                const IoTDNP::DatabaseNodeAddrStruct* addr)
{
    joinNodeId_ = joinNodeId;
    addr_ = *addr;
    return TransactionController::setTransaction(nodes, readyNode, ownNodeId);
}

NodeId JoinDatabaseNodeController::joinNodeId() const
{
    return joinNodeId_;
}

const ::IoTDNP::DatabaseNodeAddrStruct& JoinDatabaseNodeController::addr() const
{
    return addr_;
}

} // namespace ReplicationService
