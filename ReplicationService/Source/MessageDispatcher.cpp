/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MessageDispatcher.hpp"
#include "ConnectionContainer.hpp"
#include "Handler/InitJoinDatabaseNodeReqMsgHandlerTask.hpp"
#include "Handler/InitJoinDatabaseNodeRespMsgHandlerTask.hpp"
#include "Handler/StartJoinDatabaseNodeMsgHandlerTask.hpp"
#include "Handler/ReadyJoinDatabaseNodeMsgHandlerTask.hpp"
#include "Handler/ConnJoinDatabaseNodeReqMsgHandlerTask.hpp"
#include "Handler/ConnJoinDatabaseNodeRespMsgHandlerTask.hpp"
#include "Handler/StopJoinDatabaseNodeMsgHandlerTask.hpp"
#include "Handler/FinishJoinDatabaseNodeMsgHandlerTask.hpp"
#include "Handler/FetchDatabaseTableReqMsgHandlerTask.hpp"
#include "Handler/FetchDatabaseTableRespMsgHandlerTask.hpp"
#include "Handler/StartTransactionMsgHandlerTask.hpp"
#include "Handler/ReadyTransactionMsgHandlerTask.hpp"
#include "Handler/StopTransactionMsgHandlerTask.hpp"

#include <Util/Handler/HandlerTask.hpp>
#include <flatbuffers/flatbuffers.h>
#include <IoTDNP_generated.h>
#include <QThreadPool>

namespace ReplicationService
{
namespace MessageDispatcher
{

void dispatch(Msg::MessageBuffer&& msgBuf,
              const ReplicationConnectionManager* connManager)
{
    const ::IoTDNP::Msg* msg = ::IoTDNP::GetMsg(msgBuf.getMsgBufPtr());
    std::unique_ptr<QRunnable> task = nullptr;

    qDebug() << "ReplicationService dispatch msg "
             << ::IoTDNP::EnumNameAnyMsgUnion(msg->msg_type());

    switch (msg->msg_type())
    {
    using Util::Handler::makeHandlerTask;
    case ::IoTDNP::AnyMsgUnion_StartTransactionMsg:
        task = makeHandlerTask<Handler::StartTransactionMsgHandlerTask>(
                   std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_ReadyTransactionMsg:
        task = makeHandlerTask<Handler::ReadyTransactionMsgHandlerTask>(
                   std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_StopTransactionMsg:
        task = makeHandlerTask<Handler::StopTransactionMsgHandlerTask>(
                   std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_InitJoinDatabaseNodeReqMsg:
        task = makeHandlerTask<Handler::InitJoinDatabaseNodeReqMsgHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    case ::IoTDNP::AnyMsgUnion_InitJoinDatabaseNodeRespMsg:
        task = makeHandlerTask<Handler::InitJoinDatabaseNodeRespMsgHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    case ::IoTDNP::AnyMsgUnion_StartJoinDatabaseNodeMsg:
        task = makeHandlerTask<Handler::StartJoinDatabaseNodeMsgHandlerTask>(
                    std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_ReadyJoinDatabaseNodeMsg:
        task = makeHandlerTask<Handler::ReadyJoinDatabaseNodeMsgHandlerTask>(
                    std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_ConnJoinDatabaseNodeReqMsg:
        task = makeHandlerTask<Handler::ConnJoinDatabaseNodeReqMsgHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    case ::IoTDNP::AnyMsgUnion_ConnJoinDatabaseNodeRespMsg:
        task = makeHandlerTask<Handler::ConnJoinDatabaseNodeRespMsgHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    case ::IoTDNP::AnyMsgUnion_StopJoinDatabaseNodeMsg:
        task = makeHandlerTask<Handler::StopJoinDatabaseNodeMsgHandlerTask>(
                    std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_FinishJoinDatabaseNodeMsg:
        task = makeHandlerTask<Handler::FinishJoinDatabaseNodeMsgHandlerTask>(
                    std::move(msgBuf), msg);
        break;
    case ::IoTDNP::AnyMsgUnion_FetchDatabaseTableReqMsg:
        task = makeHandlerTask<Handler::FetchDatabaseTableReqMsgHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    case ::IoTDNP::AnyMsgUnion_FetchDatabaseTableRespMsg:
        task = makeHandlerTask<Handler::FetchDatabaseTableRespMsgHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    case ::IoTDNP::AnyMsgUnion_NONE:
        qCritical() << "ReplicationService dispatch msg AnyMsgUnion_NONE";
        break;
    default:
        qInfo() << "ReplicationService dispatch unknown "
                << ::IoTDNP::EnumNameAnyMsgUnion(msg->msg_type())
                << " (" << msg->msg_type() << ") message!";
    }
    if(task)
    {
        QThreadPool::globalInstance()->start(task.release());
    }
}

} // namespace MessageDispatcher
} // namespace ReplicationService
