/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_REPLICATIONCONNECTIONMANAGER_HPP
#define REPLICATIONSERVICE_REPLICATIONCONNECTIONMANAGER_HPP

#include <memory>
#include <QObject>
#include <QTcpSocket>
#include <Worker.hpp>
#include <MessageTypes.hpp>
#include <MessageBuffer.hpp>
#include <QHostAddress>
#include <QMetaType>
#include <MessageSink.hpp>

enum ConnectionState
{
    UnconnectedState = QAbstractSocket::UnconnectedState,	//0	The socket is not connected.
    HostLookupState = QAbstractSocket::HostLookupState,	//1	The socket is performing a host name lookup.
    ConnectingState = QAbstractSocket::ConnectingState,	//2	The socket has started establishing a connection.
    ConnectedState = QAbstractSocket::ConnectedState,	//3	A connection is established.
    BoundState = QAbstractSocket::BoundState,	  //4	The socket is bound to an address and port.
    ClosingState = QAbstractSocket::ClosingState,	//6	The socket is about to close (data may still be waiting to be written).
    ListeningState = QAbstractSocket::ListeningState, //5	For internal use only.
};

Q_DECLARE_METATYPE( ConnectionState )
const int typeIdConnectionState = qRegisterMetaType< ConnectionState >( "ConnectionState" );

namespace ReplicationService
{


class ReplicationConnectionManager : public QObject, public Parallel::Worker
{
    Q_OBJECT
    Q_INTERFACES(Parallel::Worker)
public:
    ReplicationConnectionManager(const ReplicationConnectionManager&) = delete;
    ReplicationConnectionManager& operator=(const ReplicationConnectionManager&) = delete;
    ReplicationConnectionManager(ReplicationConnectionManager&&) = delete;
    ReplicationConnectionManager& operator=(ReplicationConnectionManager&&) = delete;
    explicit ReplicationConnectionManager(const QHostAddress& addr, const quint16 port);
    explicit ReplicationConnectionManager(const qintptr socketDescriptor);
    ~ReplicationConnectionManager() override;

signals:
    void finished(const Parallel::Worker*) override;
    void deleted(const ReplicationConnectionManager*);
    void stateChanged(const ReplicationConnectionManager* conn,
                      const ConnectionState state);
    void establishConnection(const ReplicationConnectionManager* conn,
                      const ConnectionState state);

public slots:
    void doWork() override;
    void quit() override;
    void onConnected();
    void onDisconnect();
    void error(QAbstractSocket::SocketError socketError);
    void onReadyRead();
    void doWrite(IMsgPtr msg);

private:
    void receiveMsg(Msg::MessageBuffer&& msgBuf);

private:
    qintptr socketDescriptor_;
    QHostAddress addr_;
    quint16 port_;
    std::unique_ptr<QTcpSocket> socket_;
    std::unique_ptr<Msg::MessageSink> sink_;
};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_REPLICATIONCONNECTIONMANAGER_HPP
