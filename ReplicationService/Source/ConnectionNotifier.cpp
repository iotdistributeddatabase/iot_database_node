/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConnectionNotifier.hpp"
#include "ConnectionContainer.hpp"
#include <QDebug>

namespace ReplicationService
{

ConnectionNotifier::ConnectionNotifier(AddConnectionFun fun)
    : addConnection_(fun)
{

}

void ConnectionNotifier::destroyedConnection(const ReplicationConnectionManager *conn)
{
    qDebug() << "ConnectionNotifier::destroyedConnection conn=" << static_cast<const void*>(conn);
    ConnectionContainer::globalInstance().removeConnection(conn);
}

void ConnectionNotifier::addConnection(IConnectionInitiatorPtr init)
{
    QObject* p = addConnection_(init->addr(), init->port());
    auto push = connInits_.insert({p, init});
    if(not push.second)
    {
        qCritical() << "ConnectionNotifier could not add connection"
                    << "addr=" << init->addr() << "port=" << init->port();
    }
}

void ConnectionNotifier::establishConnection(const ReplicationConnectionManager *conn,
                                             const ConnectionState state)
{
    auto search = connInits_.find(conn);
    if(search != connInits_.end())
    {
        search->second->handleConnectionSetup(conn, state);
        connInits_.erase(search);
    }
    else
    {
        qWarning() << "ConnectionNotifier::establishConnection dangling conn=" << static_cast<const void*>(conn);
    }
}

} // namespace ReplicationService
