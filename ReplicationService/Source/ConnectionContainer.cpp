/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ConnectionContainer.hpp"
#include <algorithm>
#include <QObject>
#include "ReplicationServiceConstants.hpp"
#include "ReplicationServer.hpp"

namespace ReplicationService
{

ConnectionContainer::ConnectionContainer(const std::size_t size)
    : conns_(), transactionNodes_(ReplicationServer::globalNodeId)
{
    conns_.reserve(size);
}

void ConnectionContainer::addConnection(const ConnectionInfo& conn)
{
    std::lock_guard guard{mutex_};
    auto search = std::find_if(conns_.begin(), conns_.end(),
                               [&conn](const ConnectionInfo& connData) -> bool
                               { return connData.connManager == conn.connManager; });
    if(search == conns_.end())
    {
        conns_.push_back(conn);
        qDebug() << "ConnectionContainer::addConnection conn=" << static_cast<const void*>(conn.connManager)
                 <<"nodeId=" << conn.nodeId;
    }
}

void ConnectionContainer::removeConnection(const ReplicationConnectionManager* conn)
{
    std::lock_guard guard{mutex_};
    auto search = std::find_if(conns_.begin(), conns_.end(),
                               [conn](const ConnectionInfo& connData) -> bool
                               { return connData.connManager == conn; });
    if(search != conns_.end())
    {
        if(transactionNodes_ & search->nodeId)
        {
            transactionNodes_ ^= search->nodeId;
        }
        qDebug() << "ConnectionContainer::removeConnection nodeId=" << search->nodeId;
        conns_.erase(search);
        qDebug() << "ConnectionContainer::removeConnection conn=" << static_cast<const void*>(conn);
    }
}

Nodes ConnectionContainer::transactionNodes()
{
    std::lock_guard guard{mutex_};
    return transactionNodes_;
}

void ConnectionContainer::addTransactionNode(const NodeId nodeId)
{
    std::lock_guard guard{mutex_};
    transactionNodes_ |= nodeId;
}

const ConnectionNotifier *ConnectionContainer::getConnNotiffier() const
{
    return connNotiffier_;
}

void ConnectionContainer::setConnNotiffier(const ConnectionNotifier* value)
{
    connNotiffier_.store(value);
}

ConnectionContainer& ConnectionContainer::globalInstance()
{
    static ConnectionContainer container{maxNodes};
    return container;
}

} // namespace ReplicationService
