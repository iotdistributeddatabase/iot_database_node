/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_TABLECONTROLLER_HPP
#define REPLICATIONSERVICE_TABLECONTROLLER_HPP

#include <unordered_map>
#include <memory>
#include <mutex>
#include "TableTransactionController.hpp"
#include "TransactionId.hpp"
#include <PriorityQueue.hpp>

namespace ReplicationService
{

class TableController
{
public:
    using TransactionControllerPtr = std::shared_ptr<TableTransactionController>;
    using TableTransactionMap = std::map<TransactionId, TransactionControllerPtr, CompareTransactionId>;
    using TransactionPair = TableTransactionMap::value_type;
    template<typename... Args>
    static TransactionControllerPtr makeTransactionController( Args&&... args );

    explicit TableController() = default;
    TableController(const TableController&) = delete;
    TableController& operator=(const TableController&) = delete;
    TableController(TableController&&) = delete;
    TableController& operator=(TableController&&) = delete;

    bool addTransaction(TransactionId id, TransactionControllerPtr t);
    TransactionControllerPtr getTransaction(TransactionId id);
    TransactionControllerPtr getOrAddIfNotExistTransaction(TransactionId id);
    void removeTransaction(TransactionId id);
    TransactionPair topReadyTransaction();
    TransactionPair popCommitTransaction();
    void popTransaction();

private:
    std::mutex mutex_;
    std::map<TransactionId, TransactionControllerPtr, CompareTransactionId> transactions_;
};

template<typename... Args>
TableController::TransactionControllerPtr
TableController::makeTransactionController( Args&&... args )
{
    using ElementType = TransactionControllerPtr::element_type;
    return std::make_shared<ElementType>(std::forward<Args>(args)...);
}

}// namespace ReplicationService


#endif // REPLICATIONSERVICE_TABLECONTROLLER_HPP
