/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "JoinDatabaseNodeQueue.hpp"
#include "ConnectionContainer.hpp"

namespace ReplicationService
{

JoinDatabaseNodeQueue::AddOutput JoinDatabaseNodeQueue::addTransaction(const TransactionId id,
                                           JoinDatabaseNodeControllerPtr t)
{
    std::lock_guard guard{mutex_};
    Nodes nodes = ConnectionContainer::globalInstance().transactionNodes();
    t->updateNodes(nodes);
    auto push = joinQueue_.insert({id, t});
    return JoinDatabaseNodeQueue::AddOutput {push.second, nodes};
}

JoinDatabaseNodeQueue::JoinDatabaseNodeControllerPtr
JoinDatabaseNodeQueue::getTransaction(const TransactionId id)
{
    std::lock_guard guard{mutex_};
    auto search = joinQueue_.find(id);
    if(search != joinQueue_.end())
    {
        return search->second;
    }
    return nullptr;
}

JoinDatabaseNodeQueue::JoinDatabaseNodeControllerPtr
JoinDatabaseNodeQueue::getOrAddIfNotExistTransaction(const TransactionId id)
{
    std::lock_guard guard{mutex_};
    auto search = joinQueue_.find(id);
    if(search != joinQueue_.end())
    {
        return search->second;
    }
    else
    {
        JoinDatabaseNodeControllerPtr p = makeJoinDatabaseNodeController();
        auto push = joinQueue_.insert({id, p});
        if(push.second)
        {
            return p;
        }
        else
        {
            return nullptr;
        }
    }
}

void JoinDatabaseNodeQueue::removeTransaction(const TransactionId id)
{
    std::lock_guard guard{mutex_};
    joinQueue_.erase(id);
}

JoinDatabaseNodeQueue::JoinTransactionPair JoinDatabaseNodeQueue::topReadyTransaction()
{
    std::lock_guard guard{mutex_};
    if(not joinQueue_.empty())
    {
        auto b = joinQueue_.begin();
        if(b->second->isReadyTransaction())
        {
            return *b;
        }
    }
    return {nullTransactionId, nullptr};
}

JoinDatabaseNodeQueue::PopOutput JoinDatabaseNodeQueue::popCommitTransaction()
{
    std::lock_guard guard{mutex_};
    if(joinQueue_.empty())
    {
        return PopOutput {};
    }
    else
    {
        auto [id, trans] = *joinQueue_.begin();
        if(trans->isCommitTransaction())
        {
            joinQueue_.erase(joinQueue_.begin());
            PopOutput::JoinDatabaseNodeControllerPtrs transactions {};
            if(trans->isCommited())
            {
                ConnectionContainer::globalInstance()
                        .addTransactionNode(trans->joinNodeId());
                transactions.reserve(joinQueue_.size());
                for(auto& t : joinQueue_)
                {
                    t.second->updateNodes(trans->joinNodeId());
                    transactions.push_back(t);
                }
            }
            return PopOutput {id, trans, std::move(transactions)};
        }
        else
        {
            return PopOutput {};
        }
    }
}

JoinDatabaseNodeQueue &JoinDatabaseNodeQueue::globalInstance()
{
    static JoinDatabaseNodeQueue joinDbNodeQueue {};
    return joinDbNodeQueue;
}

} // namespace ReplicationService
