/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_CONNECTIONNOTIFIER_HPP
#define REPLICATIONSERVICE_CONNECTIONNOTIFIER_HPP

#include "ReplicationConnectionManager.hpp"
#include "IConnectionInitiator.hpp"
#include <QObject>
#include <QHostAddress>
#include <functional>

namespace ReplicationService
{

typedef std::function<QObject*(const QHostAddress& address,
                               const quint16 port)> AddConnectionFun;

class ConnectionNotifier : public QObject
{
    Q_OBJECT
public:
    explicit ConnectionNotifier(AddConnectionFun fun);

signals:

public slots:
    void destroyedConnection(const ReplicationConnectionManager* conn);
    void addConnection(IConnectionInitiatorPtr init);
    void establishConnection(const ReplicationConnectionManager* conn,
                             const ConnectionState state);

private:
    std::unordered_map<const QObject*, IConnectionInitiatorPtr> connInits_;
    AddConnectionFun addConnection_;
};

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_CONNECTIONNOTIFIER_HPP
