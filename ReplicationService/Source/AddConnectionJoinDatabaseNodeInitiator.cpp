/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "AddConnectionJoinDatabaseNodeInitiator.hpp"
#include "Handler/ConnectionSetupJoinDatabaseNodeHandlerTask.hpp"
#include <QObject>
#include <QThreadPool>

namespace ReplicationService
{

AddConnectionJoinDatabaseNodeInitiator::AddConnectionJoinDatabaseNodeInitiator(
        const QHostAddress &addr,
        const quint16 port,
        const TransactionId &transId)
    : IConnectionInitiator (addr, port), transId_(transId)
{

}

void AddConnectionJoinDatabaseNodeInitiator::handleConnectionSetup(const ReplicationConnectionManager* conn,
                                                   const ConnectionState state)
{
    std::unique_ptr<Handler::ConnectionSetupJoinDatabaseNodeHandlerTask> task
            = std::make_unique<Handler::ConnectionSetupJoinDatabaseNodeHandlerTask>(
                transId_, conn, state);
    QThreadPool::globalInstance()->start(task.release());
}

} // namespace ReplicationService
