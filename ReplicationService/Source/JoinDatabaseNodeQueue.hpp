/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef REPLICATIONSERVICE_JOINDATABASENODEQUEUE_HPP
#define REPLICATIONSERVICE_JOINDATABASENODEQUEUE_HPP

#include "TransactionId.hpp"
#include "JoinDatabaseNodeController.hpp"
#include <mutex>
#include <map>
#include <memory>

namespace ReplicationService
{

class JoinDatabaseNodeQueue
{
public:
    using JoinDatabaseNodeControllerPtr = std::shared_ptr<JoinDatabaseNodeController>;
    using JoinDatabaseNodeMap = std::map<TransactionId, JoinDatabaseNodeControllerPtr,
                                         CompareTransactionId>;
    using JoinTransactionPair = JoinDatabaseNodeMap::value_type;

    template<typename... Args>
    static JoinDatabaseNodeControllerPtr makeJoinDatabaseNodeController(Args&&...args);

    struct AddOutput
    {
        bool alloc;
        Nodes nodes;
    };
    struct PopOutput
    {
        using JoinDatabaseNodeControllerPtrs = std::vector<JoinTransactionPair>;
        TransactionId id;
        JoinDatabaseNodeControllerPtr trans;
        JoinDatabaseNodeControllerPtrs transactions;
    };

public:
    explicit JoinDatabaseNodeQueue() = default;
    JoinDatabaseNodeQueue(const JoinDatabaseNodeQueue&) = delete;
    JoinDatabaseNodeQueue& operator=(const JoinDatabaseNodeQueue&) = delete;
    JoinDatabaseNodeQueue(JoinDatabaseNodeQueue&&) = delete;
    JoinDatabaseNodeQueue& operator=(JoinDatabaseNodeQueue&&) = delete;

    AddOutput addTransaction(const TransactionId id,
                        JoinDatabaseNodeControllerPtr t);
    JoinDatabaseNodeControllerPtr getTransaction(const TransactionId id);
    JoinDatabaseNodeControllerPtr getOrAddIfNotExistTransaction(const TransactionId id);
    void removeTransaction(const TransactionId id);

    JoinTransactionPair topReadyTransaction();
    PopOutput popCommitTransaction();

    static JoinDatabaseNodeQueue& globalInstance();

private:
    std::mutex mutex_;
    JoinDatabaseNodeMap joinQueue_;
};

template<typename... Args>
JoinDatabaseNodeQueue::JoinDatabaseNodeControllerPtr
JoinDatabaseNodeQueue::makeJoinDatabaseNodeController(Args&&...args)
{
    using ElementType = JoinDatabaseNodeControllerPtr::element_type;
    return std::make_shared<ElementType>(std::forward<Args>(args)...);
}

} // namespace ReplicationService

#endif // REPLICATIONSERVICE_JOINDATABASENODEQUEUE_HPP
