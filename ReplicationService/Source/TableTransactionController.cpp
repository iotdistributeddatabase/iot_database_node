/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "TableTransactionController.hpp"
#include <QtSql/QSqlQuery>
#include <DbConnectionPool.hpp>
#include <QDebug>
#include <QtSql/QSqlError>

namespace ReplicationService
{

TableTransactionController::TableTransactionController()
    : TransactionController(), cmd_(), handleTransFinish_()
{
}

TableTransactionController::TableTransactionController(const Nodes nodes,
                                                       const NodeId ownNodeId,
                                                       const char* const cmd,
                                                       const HandleTransactionFinish handle)
    : TransactionController(nodes, ownNodeId), cmd_(cmd),
      handleTransFinish_(handle)
{
}

bool TableTransactionController::setTransaction(const Nodes nodes,
                                                const NodeId ownNodeId,
                                                const char* const cmd,
                                                const NodeId readyNode)
{
    cmd_ = cmd;
    return TransactionController::setTransaction(nodes, ownNodeId, readyNode);
}


::IoTDNP::CommitStatusEnum TableTransactionController::readyTransaction()
{
    auto db = Database::DbConnectionPool::globalInstance()->getConnection();
    if(not db.transaction())
    {
        qCritical() << "DB could not begin ready transaction" << db.lastError().text();
        return ::IoTDNP::CommitStatusEnum_CRASHED;
    }
    QSqlQuery query{db};
    bool result = query.exec(cmd_.c_str());
    if(not db.rollback())
    {
        qCritical() << "DB could not end ready transaction" << db.lastError().text();
        return ::IoTDNP::CommitStatusEnum_CRASHED;
    }
    if(result)
    {
        return ::IoTDNP::CommitStatusEnum_COMMITED;
    }
    else
    {
        qWarning() << "DB could not execute ready transaction" << db.lastError().text();
        ifPresentHandleTransFinish(false, db.lastError().text().toUtf8());
        return ::IoTDNP::CommitStatusEnum_ABORTED;
    }
}

bool TableTransactionController::commitTransaction()
{
    auto db = Database::DbConnectionPool::globalInstance()->getConnection();
    if(not db.transaction())
    {
        qCritical() << "DB could not begin transaction" << db.lastError().text();
        return false;
    }
    QSqlQuery query{db};
    bool result = query.exec(cmd_.c_str());
    if(not db.commit())
    {
        qCritical() << "DB could not commit transaction" << db.lastError().text();
        return false;
    }
    if(result)
    {
        ifPresentHandleTransFinish(true, "");
        return true;
    }
    else
    {
        qCritical() << "DB transaction faild!" << db.lastError().text();
        ifPresentHandleTransFinish(false, db.lastError().text().toUtf8());
        return false;
    }
}

void TableTransactionController::ifPresentHandleTransFinish(bool result, const char *error)
{
    if(handleTransFinish_)
    {
        handleTransFinish_(result, error);
    }
}

} // namespace ReplicationService
