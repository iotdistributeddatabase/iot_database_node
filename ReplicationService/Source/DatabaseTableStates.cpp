/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "DatabaseTableStates.hpp"

namespace ReplicationService
{

void DatabaseTableStates::incrementTableState(const std::string& table)
{
    std::lock_guard guard {mutex_};
    auto search = tableStates_.find(table);
    if(search != tableStates_.end())
    {
        ++search->second;
    }
    else
    {
        tableStates_.insert({table, 1});
    }
}

bool DatabaseTableStates::insertTableState(const std::string &table, const DatabaseTableStates::TableTransId tableTransId)
{
    std::lock_guard guard {mutex_};
    auto insert = tableStates_.insert({table, tableTransId});
    return insert.second;
}

IoTDNP::TransactionIdStruct DatabaseTableStates::saveTableStatesToMsg(Msg::IoTDNP::FetchDatabaseStateRespMsgBuilder *msg)
{
    std::lock_guard guard {mutex_};
    msg->createUninitializedVector(tableStates_.size());
    for(auto& t : tableStates_)
    {
        msg->pushTableState(t.first.c_str(), t.second);
    }
    return makeTransactionIdStruct(newestTransactionId_);
}

DatabaseTableStates& DatabaseTableStates::globalInstance()
{
    static DatabaseTableStates dbTableStates;
    return dbTableStates;
}

} // namespace ReplicationService
