/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef USERSERVICE_SENDERMSGSIGNAL_HPP
#define USERSERVICE_SENDERMSGSIGNAL_HPP

#include <MessageTypes.hpp>
#include <QObject>

namespace UserService
{

class SenderMsgSignal : public QObject
{
    Q_OBJECT
public:

signals:
    void readyWrite(IMsgPtr msg);

public:
    void emitReadyWrite(IMsgPtr msg);

    template<typename Receiver, typename Slot>
    void connectReceiver(Receiver* receiver, Slot slot)
    {
        QObject::connect(this, SIGNAL(readyWrite(IMsgPtr)),
                receiver, slot);
    }
};

} // namespace UserService

#endif // USERSERVICE_SENDERMSGSIGNAL_HPP
