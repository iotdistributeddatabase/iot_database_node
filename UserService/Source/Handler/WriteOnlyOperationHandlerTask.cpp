/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "WriteOnlyOperationHandlerTask.hpp"
#include <IoTDNPUserMsgPrototypes.hpp>
#include <ReplicationHandlers.hpp>
#include <SenderMsgSignal.hpp>
#include <Util/UniquePtrWithDeleteLater.hpp>

namespace UserService
{
namespace Handler
{

namespace
{

struct SendDatabaseTransactionResult
{
    SendDatabaseTransactionResult(const Network::ConnectionManager* const user)
        : user_(user)
    {
    }
    void operator()(const bool result, const char* error) const
    {
        auto sender = Util::makeUniquePtrWithDeleteLater<SenderMsgSignal>();
        // QObject::connect(sender.get(), &Util::SenderMsgSignal::readyWrite,
        //                  user_, &UserConnectionManager::doWrite);

        Network::connectToConnectionManager(sender.get(),
                                            SIGNAL(readyWrite(IMsgPtr)), user_);

        auto msg = makeIMsgPtr<Msg::IoTDNP::OperationRespUserMsgPrototype>(
                    error,
                    result ? ::IoTDNP::StatusOperationEnum_SUCCESS
                           : ::IoTDNP::StatusOperationEnum_FAILURE );
        emit sender->readyWrite(msg);
    }

private:
    const Network::ConnectionManager* const user_;
};

} // namespace anonymous


WriteOnlyOperationHandlerTask::WriteOnlyOperationHandlerTask(MsgHandleType&& msg,
        const Network::ConnectionManager* const connManager)
    : msg_(std::move(msg)), connManager_(connManager)
{

}

void WriteOnlyOperationHandlerTask::run()
{
    if(not ReplicationService::createDatabaseTransaction(
                msg_->table()->c_str(),
                msg_->sqlQuery()->c_str(),
                SendDatabaseTransactionResult {connManager_}))
    {
        Network::connectToConnectionManager(this, SIGNAL(readyWrite(IMsgPtr)),
                                            connManager_);

        auto msg = makeIMsgPtr<Msg::IoTDNP::OperationRespUserMsgPrototype>(
                    "ReplicationService could not create transaction",
                    ::IoTDNP::StatusOperationEnum_FAILURE );

        emit readyWrite(msg);
    }
}

} // namespace Handler
} // namespace UserService
