/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef USERSERVICE_HANDLER_JOINMSGHANDLERTASK_HPP
#define USERSERVICE_HANDLER_JOINMSGHANDLERTASK_HPP

#include <Util/Handler/HandlerTask.hpp>
#include <QObject>
#include <MessageBuffer.hpp>
#include <MessageTypes.hpp>
#include <MessageHandle.hpp>
#include <IoTDNPUser_generated.h>
#include <ConnectionManagerTypes.hpp>


namespace UserService
{
namespace Handler
{

class WriteOnlyOperationHandlerTask : public QObject,
                                      public Util::Handler::HandlerTask
{
    Q_OBJECT
public:
    using MsgType = ::IoTDNP::WriteOnlyOperationReqUserMsg;
    using MsgHandleType = Msg::MessageHandle<MsgType>;
    explicit WriteOnlyOperationHandlerTask(MsgHandleType&& msg,
                                           const Network::ConnectionManager* const connManager);

    void run() override;

signals:
    void readyWrite(IMsgPtr msg);

public slots:

private:
    const MsgHandleType msg_;
    const Network::ConnectionManager* const connManager_;
};

} // namespace Handler
} // namespace UserService

#endif // USERSERVICE_HANDLER_JOINMSGHANDLERTASK_HPP
