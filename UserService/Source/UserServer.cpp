/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <UserServer.hpp>
#include <memory>
#include <QObject>
#include <MessageDispatcher.hpp>

namespace UserService
{

UserServer::UserServer(const QHostAddress &address, const quint16 port,
                       const std::size_t max_threads)
    : TcpServer(address, port, max_threads)
{
}

bool UserServer::startServer()
{
    if(this->startTcpServer())
    {
        qInfo() << "User Server is listening on address"
                << address_ << "and port " << port_ << "...";
        return true;
    }
    else
    {
        qCritical() << "User Server could not start on address ="
                    << address_ << "and port =" << port_;
    }
    return false;
}

bool UserServer::stopServer()
{
    return this->stopTcpServer();
}

Network::MessageDispatcher UserServer::createMessageDispatcher()
{
    return Network::MessageDispatcher {&MessageDispatcher::dispatch};
}

} // namespace UserService
