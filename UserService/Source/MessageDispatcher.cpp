/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "MessageDispatcher.hpp"
#include "Handler/WriteOnlyOperationHandlerTask.hpp"
#include <Util/Handler/HandlerTask.hpp>
#include <flatbuffers/flatbuffers.h>
#include <IoTDNPUser_generated.h>
#include <QThreadPool>
#include <QDebug>

namespace UserService
{
namespace MessageDispatcher
{

void dispatch(Msg::MessageBuffer&& msgBuf,
              const Network::ConnectionManager* const connManager)
{
    const ::IoTDNP::UserMsg* msg = ::IoTDNP::GetUserMsg(msgBuf.getMsgBufPtr());
    std::unique_ptr<QRunnable> task = nullptr;
    switch (msg->msg_type())
    {
    using Util::Handler::makeHandlerTask;
    case ::IoTDNP::AnyUserMsgUnion_WriteOnlyOperationReqUserMsg:
        task = makeHandlerTask<Handler::WriteOnlyOperationHandlerTask>(
                    std::move(msgBuf), msg, connManager);
        break;
    default:
        qInfo() << "UserService could not handle "
                << ::IoTDNP::EnumNameAnyUserMsgUnion(msg->msg_type())
                << " (" << msg->msg_type() << ") message!";
    }
    if(task)
    {
        QThreadPool::globalInstance()->start(task.release());
    }
}

} // namespace MessageDispatcher
} // namespace UserService
