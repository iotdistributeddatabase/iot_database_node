/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef USERSERVICE_USERSERVER_HPP
#define USERSERVICE_USERSERVER_HPP

#include <TcpServer.hpp>

namespace UserService
{

class UserServer : public Network::TcpServer
{
public:
    explicit UserServer(const QHostAddress &address, const quint16 port,
                        const std::size_t max_threads);
    bool startServer();
    bool stopServer();

protected:
    Network::MessageDispatcher createMessageDispatcher() override;
};

} // namespace UserService

#endif // USERSERVICE_USERSERVER_HPP
