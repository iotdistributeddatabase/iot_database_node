/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IOTDATABASENODEDRIVER_IOTDATABASENODECONNECTION_HPP
#define IOTDATABASENODEDRIVER_IOTDATABASENODECONNECTION_HPP

#include <QTcpSocket>
#include <IoTDNPUser_generated.h>
#include <flatbuffers/flatbuffers.h>
#include "DbMsgHandle.hpp"

namespace IoTDatabaseNodeDriver
{

class IoTDatabaseNodeConnection
{
public:
    IoTDatabaseNodeConnection();

    bool connectToDatabase(const QString& address, quint16 port,
                           const QString& dbName);

    bool sendWriteOnlyOperationReqUserMsg(const QString& tableName,
                                          const QString& query);

    bool sendMsg(IoTDNP::AnyUserMsgUnion msgType,
                 flatbuffers::Offset<void> msg);
    DbMsgHandle receiveMsg();
    QString getError();

private:
    flatbuffers::FlatBufferBuilder builder_;
    QTcpSocket socket_;
    QString dbName_;
};

} // namespace IoTDatabaseNodeDriver

#endif // IOTDATABASENODEDRIVER_IOTDATABASENODECONNECTION_HPP
