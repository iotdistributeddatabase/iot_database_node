/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "IoTDatabaseNodeResult.hpp"
#include <QDebug>
#include "IoTDatabaseNodeDriver.hpp"
#include <QtSql/QSqlError>
#include <QDataStream>
#include <QVariant>
#include <QIODevice>
#include <IoTDNPUser_generated.h>

namespace IoTDatabaseNodeDriver
{

namespace
{
QString extractTableName(const QString& query)
{
    static const char insert_into[] = "INSERT INTO";
    static const char update[] = "UPDATE";
    static const char delete_from[] = "DELETE FROM";
    QString queryTmp {query.trimmed()};
    int firstIdx = 0, lastIdx = -1;
    if (queryTmp.startsWith(insert_into, Qt::CaseInsensitive))
    {
        firstIdx = sizeof (insert_into);
        int idxValues = queryTmp.indexOf("VALUES", firstIdx, Qt::CaseInsensitive);
        int idxBracket = queryTmp.indexOf("(", firstIdx, Qt::CaseInsensitive);
        lastIdx = idxValues < idxBracket ?  idxValues : idxBracket;
    }
    else if (queryTmp.startsWith(update, Qt::CaseInsensitive))
    {
        firstIdx = sizeof (update);
        lastIdx = queryTmp.indexOf("SET", firstIdx, Qt::CaseInsensitive);
    }
    else if (queryTmp.startsWith(delete_from, Qt::CaseInsensitive))
    {
        firstIdx = sizeof (delete_from);
        lastIdx = queryTmp.indexOf("WHERE", firstIdx, Qt::CaseInsensitive);
    }
    if(lastIdx > firstIdx)
    {
        return queryTmp.mid(firstIdx, lastIdx).trimmed().toUpper();
    }
    return QString {};
}
} // namespace anonymous

IoTDatabaseNodeResult::IoTDatabaseNodeResult(const QSqlDriver* driver, IoTDatabaseNodeConnection* conn)
    : QSqlResult(driver), stm_(""), conn_(conn)
{
}

IoTDatabaseNodeResult::~IoTDatabaseNodeResult()
{
}

bool IoTDatabaseNodeResult::reset(const QString &query)
{
    qDebug() << Q_FUNC_INFO;
    if(not prepare(query))
    {
        return false;
    }
    return exec();
}

bool IoTDatabaseNodeResult::prepare(const QString& query)
{
    qDebug() << Q_FUNC_INFO;
    if(not driver() or not driver()->isOpen() or driver()->isOpenError())
    {
        return false;
    }
    stm_ = query;
    setSelect(false);
    return true;
}

bool IoTDatabaseNodeResult::exec()
{
    qDebug() << Q_FUNC_INFO;
    setLastError(QSqlError());

    QString table = extractTableName(stm_);
    if(not table.size() or not stm_.size())
    {
        setLastError(QSqlError("Query is not supported: table="
                               + table + " stm=" + stm_));
        return false;
    }
    if(not conn_->sendWriteOnlyOperationReqUserMsg(table, stm_))
    {
        setLastError(QSqlError("sendQueryRequestMsg faild"));
        return false;
    }

    auto msg = conn_->receiveMsg();
    const ::IoTDNP::OperationRespUserMsg* msgResp = nullptr;

    if(msg and (msgResp = msg->msg_as_OperationRespUserMsg()))
    {
        if(msgResp->status() == ::IoTDNP::StatusOperationEnum_SUCCESS)
        {
            setLastError(QSqlError(""));
            return true;
        }
        else
        {
            setLastError(QSqlError(msgResp->result()->c_str()));
            return false;
        }
    }
    setLastError(QSqlError("Connection failed"));
    return false;
}

QVariant IoTDatabaseNodeResult::data(int /*index*/)
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return QVariant();
}

bool IoTDatabaseNodeResult::isNull(int /*index*/)
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return false;
}

bool IoTDatabaseNodeResult::fetch(int /*index*/)
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return false;
}

bool IoTDatabaseNodeResult::fetchFirst()
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return false;
}

bool IoTDatabaseNodeResult::fetchLast()
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return false;
}

int IoTDatabaseNodeResult::size()
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return 0;
}

int IoTDatabaseNodeResult::numRowsAffected()
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return 0;
}

QSqlRecord IoTDatabaseNodeResult::record() const
{
    qDebug() << Q_FUNC_INFO << "implement me";
    return QSqlRecord();
}

} // namespace IoTDatabaseNodeDriver
