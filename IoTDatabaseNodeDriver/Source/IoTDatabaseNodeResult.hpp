/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IOTDATABASENODEDRIVER_IOTDATABASENODERESULT_HPP
#define IOTDATABASENODEDRIVER_IOTDATABASENODERESULT_HPP

#include <QtSql/QSqlResult>
#include <QtSql/QSqlRecord>
#include <vector>
#include <string>

namespace IoTDatabaseNodeDriver
{

class IoTDatabaseNodeConnection;

class IoTDatabaseNodeResult : public QSqlResult
{
public:
    IoTDatabaseNodeResult(const QSqlDriver* driver, IoTDatabaseNodeConnection* conn);
    ~IoTDatabaseNodeResult();

protected:
    QVariant data(int index) override;
    bool isNull(int index) override;
    bool reset(const QString& query) override;
    bool fetch(int index) override;
    bool fetchFirst() override;
    bool fetchLast() override;
    int size() override;
    int numRowsAffected() override;
    QSqlRecord record() const override;

    bool prepare(const QString &query) override;
    bool exec() override;

protected:
    QString stm_;
    IoTDatabaseNodeConnection* conn_;
    std::vector<std::string> columns_;
    std::size_t numRows_;
    std::vector<QVariant> rows_;
};

} // namespace IoTDatabaseNodeDriver

#endif // IOTDATABASENODEDRIVER_IOTDATABASENODERESULT_HPP
