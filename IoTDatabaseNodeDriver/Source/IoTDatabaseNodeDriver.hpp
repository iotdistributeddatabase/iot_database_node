/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IOTDATABASENODEDRIVER_IOTDATABASENODEDRIVER_HPP
#define IOTDATABASENODEDRIVER_IOTDATABASENODEDRIVER_HPP

#include <QtSql/QSqlDriver>
#include "IoTDatabaseNodeConnection.hpp"

namespace IoTDatabaseNodeDriver
{

class IoTDatabaseNodeDriver : public QSqlDriver
{
public:
    IoTDatabaseNodeDriver();
    ~IoTDatabaseNodeDriver();

    bool hasFeature(DriverFeature feature) const override;
    bool open(const QString& db, const QString& user,
              const QString& password, const QString& host,
              int port, const QString& connOptions) override;
    void close() override;
    QSqlResult* createResult() const override;

    bool beginTransaction() override;
    bool commitTransaction() override;
    bool rollbackTransaction() override;

private:
    QString dbName_;
    IoTDatabaseNodeConnection conn_;
};

} // namespace IoTDatabaseNodeDriver

#endif // IOTDATABASENODEDRIVER_IOTDATABASENODEDRIVER_HPP
