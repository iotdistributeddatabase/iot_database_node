/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "IoTDatabaseNodeDriver.hpp"
#include "IoTDatabaseNodeResult.hpp"
#include <QDebug>
#include "IoTDatabaseNodeConnection.hpp"
#include <QSqlError>

namespace IoTDatabaseNodeDriver
{

IoTDatabaseNodeDriver::IoTDatabaseNodeDriver()
    : conn_()
{

}

IoTDatabaseNodeDriver::~IoTDatabaseNodeDriver()
{
    qDebug() << "~IoTDatabaseNodeDriver() "  << dbName_;
}

bool IoTDatabaseNodeDriver::hasFeature(QSqlDriver::DriverFeature feature) const
{
    switch (feature)
    {
    case Transactions:
    case BLOB:
    case Unicode:
    case LastInsertId:
    case PreparedQueries:
    case PositionalPlaceholders:
    case SimpleLocking:
    case FinishQuery:
    case LowPrecisionNumbers:
    case EventNotifications:
    case QuerySize:
    case BatchOperations:
    case MultipleResultSets:
    case CancelQuery:
    case NamedPlaceholders:
        return false;
    }
    return false;
}

bool IoTDatabaseNodeDriver::open(const QString& db, const QString& user,
                              const QString& password, const QString& host,
                              int port, const QString& connOptions)
{
    setOpen(false);
    dbName_ = db;
    qDebug() << "IoTDatabaseNodeDriver::open"
             << "DatabaseName =" << db
             << "HostName =" << host
             << "PortDb =" << port
             << "UserName =" << user
             << "Password =" << password
             << "ConnectOptions =" << connOptions;

    if(not conn_.connectToDatabase(host, static_cast<quint16>(port), dbName_))
    {
        setOpen(false);
        setOpenError(true);
        setLastError(conn_.getError());
        return false;
    }

    setOpen(true);
    return true;
}

void IoTDatabaseNodeDriver::close()
{
    qDebug() << "IoTDatabaseNodeDriver::close() "  << dbName_;
}

QSqlResult *IoTDatabaseNodeDriver::createResult() const
{
    return new IoTDatabaseNodeResult(this, const_cast<IoTDatabaseNodeConnection*>(&conn_));
}

bool IoTDatabaseNodeDriver::beginTransaction()
{
    qCritical() << Q_FUNC_INFO << " no implementation";
    return false;
}

bool IoTDatabaseNodeDriver::commitTransaction()
{
    qCritical() << Q_FUNC_INFO << " no implementation";
    return false;
}

bool IoTDatabaseNodeDriver::rollbackTransaction()
{
    qCritical() << Q_FUNC_INFO << " no implementation";
    return false;
}

} // namespace IoTDatabaseNodeDriver
