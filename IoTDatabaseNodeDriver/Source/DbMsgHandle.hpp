/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef IOTDATABASENODEDRIVER_DBMSGHANDLE_HPP
#define IOTDATABASENODEDRIVER_DBMSGHANDLE_HPP

#include <memory>
#include <cstdint>
#include <IoTDNPUser_generated.h>

namespace IoTDatabaseNodeDriver
{

class DbMsgHandle
{
public:
    DbMsgHandle(const DbMsgHandle&) = delete;
    DbMsgHandle& operator=(const DbMsgHandle&) = delete;
    DbMsgHandle(DbMsgHandle&& r)
        : msgBuf_(r.msgBuf_.release()), size_(std::exchange(r.size_, 0)),
          msg_(std::exchange(r.msg_, nullptr))
    {
    }
    DbMsgHandle& operator=(DbMsgHandle&& r)
    {
        DbMsgHandle tmp{std::move(r)};
        std::swap(msgBuf_, tmp.msgBuf_);
        std::swap(size_, tmp.size_);
        std::swap(msg_, tmp.msg_);
        return *this;
    }

    DbMsgHandle()
        : msgBuf_(nullptr), size_(0),
          msg_(nullptr)
    {}
    DbMsgHandle(const std::size_t size)
        : msgBuf_(std::make_unique<char []>(size)), size_(size),
          msg_(nullptr)
    {}
    char* getMsgBufPtr() const
    {
        return msgBuf_.get();
    }
    std::size_t getSize() const
    {
        return size_;
    }
    void setMsg()
    {
        msg_ = IoTDNP::GetUserMsg(msgBuf_.get());
    }
    const IoTDNP::UserMsg* operator->() const
    {
        return msg_;
    }
    const IoTDNP::UserMsg& operator*() const
    {
        return *msg_;
    }
    operator bool() const
    {
        return msg_ != nullptr;
    }

private:
    std::unique_ptr<char[]> msgBuf_{};
    std::size_t size_{};
    const IoTDNP::UserMsg* msg_{};
};

} // namespace IoTDatabaseNodeDriver

#endif // IOTDATABASENODEDRIVER_DBMSGHANDLE_HPP
