/*
 * IoTDatabaseNodeDriver
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "IoTDatabaseNodeConnection.hpp"
#include <IoTDNPUser_generated.h>

namespace IoTDatabaseNodeDriver
{

IoTDatabaseNodeConnection::IoTDatabaseNodeConnection()
    : builder_(1024), socket_()
{
}

bool IoTDatabaseNodeConnection::connectToDatabase(const QString&address,
                                                  quint16 port,
                                                  const QString& dbName)
{
    dbName_ = dbName;
    socket_.connectToHost(address, port);
    if(not socket_.waitForConnected(-1))
    {
        qCritical() << "IoTDatabaseNodeDriver could not connect error="
                    << socket_.error() << "\nDbName =" << dbName_;
        return false;
    }
    return true;
}

bool IoTDatabaseNodeConnection::sendWriteOnlyOperationReqUserMsg(const QString& tableName,
                                                                 const QString& query)
{
    auto table = builder_.CreateString(tableName.toStdString());
    auto cmd = builder_.CreateString(query.toStdString());
    auto msg = ::IoTDNP::CreateWriteOnlyOperationReqUserMsg(builder_, table, cmd);
    return sendMsg(::IoTDNP::AnyUserMsgUnion_WriteOnlyOperationReqUserMsg, msg.Union());
}

bool IoTDatabaseNodeConnection::sendMsg(IoTDNP::AnyUserMsgUnion msgType,
                                        flatbuffers::Offset<void> msg)
{
    auto msgBuilder = ::IoTDNP::CreateUserMsg(builder_, msgType, msg);
    builder_.FinishSizePrefixed(msgBuilder);
    auto sendLen = socket_.write(
                reinterpret_cast<char*>(builder_.GetBufferPointer()),
                builder_.GetSize());
    qDebug() << "IoTDatabaseNodeDriver: send msg with size=" << sendLen;
    if(sendLen != builder_.GetSize() or !socket_.flush())
    {
        qCritical()
                << "IoTDatabaseNodeDriver could not send a whole msg error="
                << socket_.error() << "\nDbName =" << dbName_;
        return false;
    }
    builder_.Clear();
    return true;
}

DbMsgHandle IoTDatabaseNodeConnection::receiveMsg()
{
    if(!socket_.waitForReadyRead())
    {
        qCritical()
                << "IoTDatabaseNodeDriver could not read a msg error="
                << socket_.error() << "\nDbName =" << dbName_;
        return DbMsgHandle();
    }
    flatbuffers::uoffset_t size_;
    qint64 readLen = socket_.read(reinterpret_cast<char*>(&size_), sizeof(size_));
    auto size = flatbuffers::ReadScalar<flatbuffers::uoffset_t>(&size_);
    if(readLen != sizeof(size_))
    {
        qCritical()
                << "IoTDatabaseNodeDriver could not receive a whole msg size error="
                << socket_.error() << "\nDbName =" << dbName_;
        return DbMsgHandle();
    }
    qDebug() << "IoTDatabaseNodeDriver: receive msg with size=" << size
             << "size bytes =" << size_;

    DbMsgHandle msgHandle {size};
    readLen = socket_.read(msgHandle.getMsgBufPtr(), size);
    qDebug() << "IoTDatabaseNodeDriver: receive readLen=" << readLen;
    if(readLen != size)
    {
        qCritical()
                << "IoTDatabaseNodeDriver could not receive a whole msg error="
                << socket_.error() << "\nDbName =" << dbName_;
        return DbMsgHandle();
    }
    msgHandle.setMsg();
    return msgHandle;
}

QString IoTDatabaseNodeConnection::getError()
{
    return socket_.errorString();
}

} // namespace IoTDatabaseNodeDriver
