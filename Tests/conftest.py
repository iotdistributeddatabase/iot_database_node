# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

import pytest
from message_framework.message_sink_client import  message_sink_client_t
from message_framework.message_sink_server import  message_sink_server_t
from message_framework.system_under_test import  system_under_test_t
from message_framework.connection_client import  connection_client_t
from message_framework.print_info import  plantuml_pytest_addoption, \
                                          plantuml_pytest_configure
from FakeDatabaseDriverFramework.fake_database import fake_database_t
import os


def pytest_runtest_setup(item):
    pass


def pytest_runtest_teardown(item):
    pass


@pytest.yield_fixture(scope="function")
def iot_database_node_sut():
    BUILD_APP_PATH = os.environ['APPLICATION_OUT_DIR']
    TEST_CONFIG_PATH = os.environ['APPLICATION_TEST_CONFIG_DIR']
    cmd = BUILD_APP_PATH + '/IoT_Database_Node ' \
        + TEST_CONFIG_PATH + '/Application.ini'
    sut = system_under_test_t("iot_database_node_sut", cmd, "::1", 7700)
    sut.start()
    yield sut
    sut.stop()


@pytest.fixture(scope="function")
def iot_database_node_sut_user():
    return connection_client_t("iot_database_node_sut_user", "::1", 8800)


@pytest.yield_fixture(scope="function")
def iot_database_node_join_sut():
    BUILD_APP_PATH = os.environ['APPLICATION_OUT_DIR']
    TEST_CONFIG_PATH = os.environ['APPLICATION_TEST_CONFIG_DIR']
    cmd = BUILD_APP_PATH + '/IoT_Database_Node ' \
        + TEST_CONFIG_PATH + '/ApplicationJoin.ini'
    sut = system_under_test_t("iot_database_node_sut", cmd, "::1", 7700)
    sut.start()
    yield sut
    sut.stop()


@pytest.fixture(scope="function")
def iot_database_node_server_2():
    return message_sink_server_t("iot_database_node_2", "::1", 8700, 100)


@pytest.fixture(scope="function")
def iot_database_node_server_3():
    return message_sink_server_t("iot_database_node_3", "::1", 9700, 100)


@pytest.yield_fixture(scope="function")
def fake_database():
    db = fake_database_t("FakeDatabase", "::1", 9900, "mydb", "admin",
                         "testpass", "test options")
    db.start()
    yield db
    db.stop()


@pytest.fixture(scope="function")
def iot_database_node_2():
    return message_sink_client_t("iot_database_node_2")


@pytest.fixture(scope="function")
def iot_database_node_3():
    return message_sink_client_t("iot_database_node_3")


@pytest.fixture(scope="function")
def iot_database_node_4():
    return message_sink_client_t("iot_database_node_4")


@pytest.fixture(scope="function")
def user():
    return message_sink_client_t("user")


def pytest_addoption(parser):
    plantuml_pytest_addoption(parser)


def pytest_configure(config):
    plantuml_pytest_configure(config)
