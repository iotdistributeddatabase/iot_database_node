# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *

def join_database_node(iot_database_node_sut, iot_database_node_2):
    iot_database_node_2.connect(iot_database_node_sut)
    #Send InitJoinDatabaseNodeReqMsg
    addr = DatabaseNodeAddrStruct_t.create(
            iot_database_node_2.get_addr(),
            iot_database_node_2.get_port())
    msg_init_req = InitJoinDatabaseNodeReqMsg_t(0x02, addr)
    iot_database_node_2.send(msg_init_req)
    #Receive InitJoinDatabaseNodeRespMsg
    addr = DatabaseNodeAddrStruct_t.create(
            iot_database_node_sut.get_addr(),
            iot_database_node_sut.get_port())
    msg_init_resp = InitJoinDatabaseNodeRespMsg_t(0x01, addr)
    iot_database_node_2.receive(msg_init_resp)
    #Receive FinishJoinDatabaseNodeMsg
    msg_finish = FinishJoinDatabaseNodeMsg_t(0x03)
    iot_database_node_2.receive(msg_finish)
