# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from message_framework.message_sink_client import  message_sink_client_t
from message_framework.connection_client import  connection_client_t
from utils.join_database_node_functions import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum


def try_receive_any(expect_msg, nodes):
    while True:
        for node in nodes:
            msg = node.try_receive(expect_msg, 0.001)
            if msg:
                return node, msg


def find_node(node_id, nodes):
    for node, addr in nodes.items():
        if node_id == addr[0]:
            return node
    return None


def compare_msg(expect_msg, msg_received):
    assert expect_msg == msg_received, \
        'The received message is not expected!\
        \n(\n expect_msg={}, \nmsg_receive={}\n)'.format(
                                             expect_msg, msg_received)


def test_joins_three_database_nodes_to_sut(iot_database_node_sut,
                                           iot_database_node_2,
                                           iot_database_node_3,
                                           iot_database_node_4):
    #JOIN DATABASE NODES
    iot_database_node_2.connect(iot_database_node_sut)
    iot_database_node_3.connect(iot_database_node_sut)
    iot_database_node_4.connect(iot_database_node_sut)
    #Send InitJoinDatabaseNodeReqMsg
    addr_2 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_2.get_addr(),
                iot_database_node_2.get_port())
    msg_init_req_2 = InitJoinDatabaseNodeReqMsg_t(0x02, addr_2)
    addr_3 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_3.get_addr(),
                iot_database_node_3.get_port())
    msg_init_req_3 = InitJoinDatabaseNodeReqMsg_t(0x04, addr_3)
    addr_4 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_4.get_addr(),
                iot_database_node_4.get_port())
    msg_init_req_4 = InitJoinDatabaseNodeReqMsg_t(0x08, addr_4)
    iot_database_node_2.send(msg_init_req_2)
    iot_database_node_3.send(msg_init_req_3)
    iot_database_node_4.send(msg_init_req_4)
    #Receive InitJoinDatabaseNodeRespMsg
    addr_1 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_sut.get_addr(),
                iot_database_node_sut.get_port())
    msg_init_resp_1 = InitJoinDatabaseNodeRespMsg_t(0x01, addr_1)
    iot_database_node_2.receive(msg_init_resp_1)
    iot_database_node_3.receive(msg_init_resp_1)
    iot_database_node_4.receive(msg_init_resp_1)
    #DATABASE NODES
    nodes = {iot_database_node_2 : (0x02, addr_2),
             iot_database_node_3 : (0x04, addr_3),
             iot_database_node_4 : (0x08, addr_4)}
    #Receive FinishJoinDatabaseNodeMsg
    node_1st, msg_finish_received = try_receive_any(FinishJoinDatabaseNodeMsg_t, nodes.keys())
    node_id_1st = nodes[node_1st][0]
    del nodes[node_1st]
    transaction_nodes = 0x01 | node_id_1st
    msg_finish = FinishJoinDatabaseNodeMsg_t(transaction_nodes)
    compare_msg(msg_finish, msg_finish_received)
    #JOIN SECOND NODE
    #Receive StartJoinDatabaseNodeMsg
    msg_start_1 = node_1st.receive_any(StartJoinDatabaseNodeMsg_t)
    msg_start_2 = node_1st.receive_any(StartJoinDatabaseNodeMsg_t)
    #SWAP IF NEEDED
    if msg_start_1.transactionId.nodeCounter == 3:
        msg_start_1, msg_start_2 = msg_start_2, msg_start_1
    nodeCounter = msg_start_1.transactionId.nodeCounter
    timeMsSinceEpoch = msg_start_1.transactionId.timeMsSinceEpoch
    trans_id_2nd = TransactionIdStruct_t(timeMsSinceEpoch, 0x01, nodeCounter)
    node_id_2nd = msg_start_1.joinNodeId
    node_2nd = find_node(node_id_2nd, nodes)
    addr_2nd = nodes[node_2nd][1]
    del nodes[node_2nd]
    msg_start_2nd = StartJoinDatabaseNodeMsg_t(0x01, trans_id_2nd, node_id_2nd, addr_2nd, transaction_nodes)
    compare_msg(msg_start_2nd, msg_start_1)
    #JOIN THIRD NODE
    node_3rd = list(nodes.keys())[0]
    node_id_3rd = nodes[node_3rd][0]
    addr_3rd = nodes[node_3rd][1]
    #Receive StartJoinDatabaseNodeMsg
    trans_id_3rd = TransactionIdStruct_t(msg_start_2.transactionId.timeMsSinceEpoch,
                                         0x01, msg_start_2.transactionId.nodeCounter)
    msg_start_3rd = StartJoinDatabaseNodeMsg_t(0x01, trans_id_3rd, node_id_3rd, addr_3rd, transaction_nodes)
    compare_msg(msg_start_3rd, msg_start_2)
    #Transaction SECOND NODE
    #Send ReadyJoinDatabaseNodeMsg
    msg_ready = ReadyJoinDatabaseNodeMsg_t(node_id_1st, trans_id_2nd, ReadyStatusEnum.PREPARED)
    node_1st.send(msg_ready)
    #Send StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(node_id_1st, trans_id_2nd, CommitStatusEnum.COMMITED)
    node_1st.send(msg_stop)
    #Receive StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(0x01, trans_id_2nd, CommitStatusEnum.COMMITED)
    node_1st.receive(msg_stop)
    #Receive FinishJoinDatabaseNodeMsg
    transaction_nodes = 0x01 | node_id_1st | node_id_2nd
    msg_finish_2nd = FinishJoinDatabaseNodeMsg_t(transaction_nodes)
    node_2nd.receive(msg_finish_2nd)
    #Transaction THIRD NODE
    #Receive StartJoinDatabaseNodeMsg
    msg_start_3rd.nodes = transaction_nodes
    node_2nd.receive(msg_start_3rd)
    #Send ReadyJoinDatabaseNodeMsg
    msg_ready = ReadyJoinDatabaseNodeMsg_t(node_id_1st, trans_id_3rd, ReadyStatusEnum.PREPARED)
    node_1st.send(msg_ready)
    msg_ready = ReadyJoinDatabaseNodeMsg_t(node_id_2nd, trans_id_3rd, ReadyStatusEnum.PREPARED)
    node_2nd.send(msg_ready)
    #Receive StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(0x01, trans_id_3rd, CommitStatusEnum.COMMITED)
    node_1st.receive(msg_stop)
    node_2nd.receive(msg_stop)
    #Send StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(node_id_1st, trans_id_3rd, CommitStatusEnum.COMMITED)
    node_1st.send(msg_stop)
    msg_stop = StopJoinDatabaseNodeMsg_t(node_id_2nd, trans_id_3rd, CommitStatusEnum.COMMITED)
    node_2nd.send(msg_stop)
    #Receive FinishJoinDatabaseNodeMsg
    transaction_nodes = 0x01 | node_id_1st | node_id_2nd | node_id_3rd
    msg_finish_3nd = FinishJoinDatabaseNodeMsg_t(transaction_nodes)
    node_3rd.receive(msg_finish_3nd)
    iot_database_node_3.destroy(timeout=1.0)
