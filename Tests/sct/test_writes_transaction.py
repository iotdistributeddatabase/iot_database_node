# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum
from utils.join_database_node_functions import *
from messages.iotdnp_user_msgs import *
from IoTDNP.StatusOperationEnum import StatusOperationEnum

def test_writes_transaction(fake_database, iot_database_node_sut, iot_database_node_sut_user,
                           user):
    # Write Transaction
    user.connect(iot_database_node_sut_user)
    write_trans = WriteOnlyOperationReqUserMsg_t('table', 'sql query')
    user.send(write_trans)
    # Database Ready Transaction
    fake_database.do_ready_transaction('sql query')
    # Database Commit Transaction
    fake_database.do_commit_transaction('sql query')
    # Finish Transaction
    finish_trans = OperationRespUserMsg_t('', StatusOperationEnum.SUCCESS)
    user.receive(finish_trans)
    user.destroy(timeout=1.0)
