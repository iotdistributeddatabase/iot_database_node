# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from message_framework.message_sink_client import  message_sink_client_t
from utils.join_database_node_functions import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum


def test_joins_two_database_nodes_to_sut(iot_database_node_sut, iot_database_node_2,
                                         iot_database_node_3):
    join_database_node(iot_database_node_sut, iot_database_node_2)
    iot_database_node_3.connect(iot_database_node_sut)
    #Send InitJoinDatabaseNodeReqMsg
    addr_3 = DatabaseNodeAddrStruct_t.create(
            iot_database_node_3.get_addr(),
            iot_database_node_3.get_port())
    msg_init_req = InitJoinDatabaseNodeReqMsg_t(0x04, addr_3)
    iot_database_node_3.send(msg_init_req)
    #Receive InitJoinDatabaseNodeRespMsg
    addr_1 = DatabaseNodeAddrStruct_t.create(
            iot_database_node_sut.get_addr(),
            iot_database_node_sut.get_port())
    msg_init_resp = InitJoinDatabaseNodeRespMsg_t(0x01, addr_1)
    iot_database_node_3.receive(msg_init_resp)
    #Receive StartJoinDatabaseNodeMsg
    trans_id = TransactionIdStruct_t(0, 0x01, 2)
    msg_start = StartJoinDatabaseNodeMsg_t(0x01, trans_id, 0x04, addr_3, 0x03)
    msg_receive_start = iot_database_node_2.receive(msg_start)
    trans_id = msg_receive_start.transactionId
    #Send ReadyJoinDatabaseNodeMsg
    msg_ready = ReadyJoinDatabaseNodeMsg_t(0x02, trans_id, ReadyStatusEnum.PREPARED)
    iot_database_node_2.send(msg_ready)
    #Send StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(0x02, trans_id, CommitStatusEnum.COMMITED)
    iot_database_node_2.send(msg_stop)
    #Receive StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(0x01, trans_id, CommitStatusEnum.COMMITED)
    iot_database_node_2.receive(msg_stop)
    #Receive FinishJoinDatabaseNodeMsg
    msg_finish = FinishJoinDatabaseNodeMsg_t(0x07)
    iot_database_node_3.receive(msg_finish)
    iot_database_node_3.destroy(timeout=1.0)
