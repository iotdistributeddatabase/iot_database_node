# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum
from utils.join_database_node_functions import *


def test_starts_transaction(fake_database, iot_database_node_sut, iot_database_node_2):
    # Join Node
    join_database_node(iot_database_node_sut, iot_database_node_2)
    # Start Transaction
    trans_id = TransactionIdStruct_t(10, 0x02, 2)
    start_trans_msg = StartTransactionMsg_t('test', trans_id, 0x02, 0x03, 'test test')
    iot_database_node_2.send(start_trans_msg)
    ready_trans_msg = ReadyTransactionMsg_t('test', trans_id, 0x01, ReadyStatusEnum.PREPARED)
    iot_database_node_2.receive(ready_trans_msg)
    # Database Ready Transaction
    fake_database.do_ready_transaction('test test')
    # Stop Transaction
    stop_trans_msg = StopTransactionMsg_t('test', trans_id, 0x01, CommitStatusEnum.COMMITED)
    iot_database_node_2.receive(stop_trans_msg)
    stop_trans_msg = StopTransactionMsg_t('test', trans_id, 0x02, CommitStatusEnum.COMMITED)
    iot_database_node_2.send(stop_trans_msg)
    # Database Commit Transaction
    fake_database.do_commit_transaction('test test')
    iot_database_node_2.destroy(timeout=1.0)
