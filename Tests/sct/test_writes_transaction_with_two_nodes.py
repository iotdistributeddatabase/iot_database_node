# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum
from utils.join_database_node_functions import *
from messages.iotdnp_user_msgs import *
from IoTDNP.StatusOperationEnum import StatusOperationEnum

def test_writes_transaction_with_two_nodes(fake_database, iot_database_node_sut, iot_database_node_sut_user,
                           iot_database_node_2, user):
    # Join Node
    join_database_node(iot_database_node_sut, iot_database_node_2)
    # Write Transaction
    user.connect(iot_database_node_sut_user)
    write_trans = WriteOnlyOperationReqUserMsg_t('table', 'sql query')
    user.send(write_trans)
    # Start Transaction
    trans_id = TransactionIdStruct_t(0, 0x01, 2)
    start_trans_msg = StartTransactionMsg_t('table', trans_id, 0x01, 0x03, 'sql query')
    received_start = iot_database_node_2.receive(start_trans_msg)
    trans_id.timeMsSinceEpoch = received_start.transactionId.timeMsSinceEpoch
    ready_trans_msg = ReadyTransactionMsg_t('table', trans_id, 0x02, ReadyStatusEnum.PREPARED)
    iot_database_node_2.send(ready_trans_msg)
    # Database Ready Transaction
    fake_database.do_ready_transaction('sql query')
    # Stop Transaction
    stop_trans_msg = StopTransactionMsg_t('table', trans_id, 0x01, CommitStatusEnum.COMMITED)
    iot_database_node_2.receive(stop_trans_msg)
    stop_trans_msg = StopTransactionMsg_t('table', trans_id, 0x02, CommitStatusEnum.COMMITED)
    iot_database_node_2.send(stop_trans_msg)
    # Database Commit Transaction
    fake_database.do_commit_transaction('sql query')
    # Finish Transaction
    finish_trans = OperationRespUserMsg_t('', StatusOperationEnum.SUCCESS)
    user.receive(finish_trans)
    iot_database_node_2.destroy(timeout=1.0)
