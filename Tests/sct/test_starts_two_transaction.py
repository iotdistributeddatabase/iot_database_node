# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum
from utils.join_database_node_functions import *


def test_starts_two_transaction(fake_database, iot_database_node_sut, iot_database_node_2):
    # Join Node
    join_database_node(iot_database_node_sut, iot_database_node_2)
    # Start Transaction 2
    trans_id_2 = TransactionIdStruct_t(10, 0x02, 2)
    start_trans_msg_2 = StartTransactionMsg_t('test', trans_id_2, 0x02, 0x03, 'test 2')
    iot_database_node_2.send(start_trans_msg_2)
    ready_trans_msg_2 = ReadyTransactionMsg_t('test', trans_id_2, 0x01, ReadyStatusEnum.PREPARED)
    iot_database_node_2.receive(ready_trans_msg_2)
    # Database Ready Transaction 2
    fake_database.do_ready_transaction('test 2')
    # Stop Transaction 2
    stop_trans_msg_2 = StopTransactionMsg_t('test', trans_id_2, 0x01, CommitStatusEnum.COMMITED)
    iot_database_node_2.receive(stop_trans_msg_2)
    # Start Transaction 1
    trans_id_1 = TransactionIdStruct_t(10, 0x02, 1)
    start_trans_msg_1 = StartTransactionMsg_t('test', trans_id_1, 0x02, 0x03, 'test 1')
    iot_database_node_2.send(start_trans_msg_1)
    ready_trans_msg_1 = ReadyTransactionMsg_t('test', trans_id_1, 0x01, ReadyStatusEnum.PREPARED)
    iot_database_node_2.receive(ready_trans_msg_1)
    # Database Ready Transaction 1
    fake_database.do_ready_transaction('test 1')
    # Stop Transaction 1
    stop_trans_msg_1 = StopTransactionMsg_t('test', trans_id_1, 0x01, CommitStatusEnum.COMMITED)
    iot_database_node_2.receive(stop_trans_msg_1)
    stop_trans_msg_1 = StopTransactionMsg_t('test', trans_id_1, 0x02, CommitStatusEnum.COMMITED)
    iot_database_node_2.send(stop_trans_msg_1)
    # Database Commit Transaction 1
    fake_database.do_commit_transaction('test 1')
    stop_trans_msg_2 = StopTransactionMsg_t('test', trans_id_2, 0x02, CommitStatusEnum.COMMITED)
    iot_database_node_2.send(stop_trans_msg_2)
    # Database Commit Transaction 2
    fake_database.do_commit_transaction('test 2')
    iot_database_node_2.destroy(timeout=1.0)
