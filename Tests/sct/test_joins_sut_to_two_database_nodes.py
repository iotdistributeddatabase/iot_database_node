# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from messages.iotdnp_msgs import *
from message_framework.message_sink_client import  message_sink_client_t
from message_framework.connection_client import  connection_client_t
from utils.join_database_node_functions import *
from IoTDNP.ReadyStatusEnum import ReadyStatusEnum
from IoTDNP.CommitStatusEnum import CommitStatusEnum


def test_joins_sut_to_two_database_nodes(iot_database_node_server_2,
                                         iot_database_node_join_sut,
                                         iot_database_node_server_3):
    #JOIN DATABASE NODE 2
    sut_conn_2 = connection_client_t("iot_database_node_sut_2")
    iot_database_node_server_2.accept_any(sut_conn_2)
    #Receive InitJoinDatabaseNodeReqMsg
    addr_1 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_join_sut.get_addr(),
                iot_database_node_join_sut.get_port())
    msg_init_req = InitJoinDatabaseNodeReqMsg_t(0x01, addr_1)
    iot_database_node_server_2.receive_from(msg_init_req, sut_conn_2)
    #Send InitJoinDatabaseNodeRespMsg
    addr_2 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_server_2.get_addr(),
                iot_database_node_server_2.get_port())
    msg_init_resp = InitJoinDatabaseNodeRespMsg_t(0x02, addr_2)
    iot_database_node_server_2.send_to(msg_init_resp, sut_conn_2)
    #Send FinishJoinDatabaseNodeMsg
    msg_finish = FinishJoinDatabaseNodeMsg_t(0x03)
    iot_database_node_server_2.send_to(msg_finish, sut_conn_2)
    #JOIN DATABASE NODE 3
    #Send StartJoinDatabaseNodeMsg
    addr_3 = DatabaseNodeAddrStruct_t.create(
                iot_database_node_server_3.get_addr(),
                iot_database_node_server_3.get_port())
    trans_id = TransactionIdStruct_t(1111, 0x02, 1)
    msg_start = StartJoinDatabaseNodeMsg_t(0x02, trans_id, 0x04, addr_3, 0x03)
    iot_database_node_server_2.send_to(msg_start, sut_conn_2)
    #Receive ReadyJoinDatabaseNodeMsg
    msg_ready = ReadyJoinDatabaseNodeMsg_t(0x01, trans_id, ReadyStatusEnum.PREPARED)
    iot_database_node_server_2.receive_from(msg_ready, sut_conn_2)
    #Send StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(0x02, trans_id, CommitStatusEnum.COMMITED)
    iot_database_node_server_2.send_to(msg_stop, sut_conn_2)
    #Receive ConnJoinDatabaseNodeReqMsg
    sut_conn_3 = connection_client_t("iot_database_node_sut_3")
    iot_database_node_server_3.accept_any(sut_conn_3)
    msg_conn_req = ConnJoinDatabaseNodeReqMsg_t(0x01, trans_id, addr_1)
    iot_database_node_server_3.receive_from(msg_conn_req, sut_conn_3)
    #Send ConnJoinDatabaseNodeRespMsg
    msg_conn_resp = ConnJoinDatabaseNodeRespMsg_t(0x04, trans_id)
    iot_database_node_server_3.send_to(msg_conn_resp, sut_conn_3)
    #Receive StopJoinDatabaseNodeMsg
    msg_stop = StopJoinDatabaseNodeMsg_t(0x01, trans_id, CommitStatusEnum.COMMITED)
    iot_database_node_server_2.receive_from(msg_stop, sut_conn_2)
    #Send StartJoinDatabaseNodeMsg
    addr_4 = DatabaseNodeAddrStruct_t.create('::1', 9999)
    trans_id = TransactionIdStruct_t(2222, 0x03, 2)
    msg_start = StartJoinDatabaseNodeMsg_t(0x03, trans_id, 0x08, addr_4, 0x07)
    iot_database_node_server_3.send_to(msg_start, sut_conn_3)
    #Receive ReadyJoinDatabaseNodeMsg
    msg_ready = ReadyJoinDatabaseNodeMsg_t(0x01, trans_id, ReadyStatusEnum.PREPARED)
    iot_database_node_server_2.receive_from(msg_ready, sut_conn_2)
    iot_database_node_server_3.receive_from(msg_ready, sut_conn_3)
    iot_database_node_server_3.destroy(timeout=1.0)
