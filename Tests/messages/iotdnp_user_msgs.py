# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import abstractmethod
from message_templates.flatbuffer_message_template \
    import flatbuffer_message_template_t, init_attrs, struct_t
from IoTDNP.UserMsg import *
from IoTDNP.AnyUserMsgUnion import *
from IoTDNP.OperationRespUserMsg import *
from IoTDNP.ReadOnlyOperationReqUserMsg import *
from IoTDNP.WriteOnlyOperationReqUserMsg import *


class user_msg_template_t(flatbuffer_message_template_t):

    __slots__ = ['type_msg']

    def __init__(self, type_msg):
        self.type_msg = type_msg

    def serialize(self, builder):
        msg = self.encode(builder)
        UserMsgStart(builder)
        UserMsgAddMsgType(builder, self.type_msg)
        UserMsgAddMsg(builder, msg)
        return UserMsgEnd(builder)

    @abstractmethod
    def encode(self, builder):
        pass

    @classmethod
    def deserialize(cls, buf, offset):
        msg_root = UserMsg.GetRootAsUserMsg(buf, offset)
        type = msg_root.MsgType()
        bytes = msg_root.Msg().Bytes
        pos = msg_root.Msg().Pos
        if type in USER_MSG_TYPES:
            msg_any, msg_temp = USER_MSG_TYPES[type]
        else:
            raise Exception("Unknow msg type id={}!".format(type))
        msg = msg_any()
        msg.Init(bytes, pos)
        return msg_temp.decode(msg)

    @classmethod
    @abstractmethod
    def decode(cls, buf, offset):
        pass


class OperationRespUserMsg_t(user_msg_template_t):

    __slots__ = ['result', 'status']

    def __init__(self, result, status):
        super().__init__(AnyUserMsgUnion.OperationRespUserMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        result = builder.CreateString(self.result)
        OperationRespUserMsgStart(builder)
        OperationRespUserMsgAddResult(builder, result)
        OperationRespUserMsgAddStatus(builder, self.status)
        return OperationRespUserMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return OperationRespUserMsg_t(msg.Result().decode(),
                                      msg.Status())


class ReadOnlyOperationReqUserMsg_t(user_msg_template_t):

    __slots__ = ['sqlQuery',]

    def __init__(self, sqlQuery):
        super().__init__(AnyUserMsgUnion.ReadOnlyOperationReqUserMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        sqlQuery = builder.CreateString(self.sqlQuery)
        ReadOnlyOperationReqUserMsgStart(builder)
        ReadOnlyOperationReqUserMsgAddSqlQuery(builder, sqlQuery)
        return ReadOnlyOperationReqUserMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return ReadOnlyOperationReqUserMsg_t(msg.SqlQuery().decode())


class WriteOnlyOperationReqUserMsg_t(user_msg_template_t):

    __slots__ = ['table', 'sqlQuery']

    def __init__(self, table, sqlQuery):
        super().__init__(AnyUserMsgUnion.WriteOnlyOperationReqUserMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        table = builder.CreateString(self.table)
        sqlQuery = builder.CreateString(self.sqlQuery)
        WriteOnlyOperationReqUserMsgStart(builder)
        WriteOnlyOperationReqUserMsgAddTable(builder, table)
        WriteOnlyOperationReqUserMsgAddSqlQuery(builder, sqlQuery)
        return WriteOnlyOperationReqUserMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return WriteOnlyOperationReqUserMsg_t(msg.Table().decode(),
                                              msg.SqlQuery().decode())


USER_MSG_TYPES = {
        AnyUserMsgUnion.OperationRespUserMsg: (OperationRespUserMsg,
                                           OperationRespUserMsg_t),
        AnyUserMsgUnion.ReadOnlyOperationReqUserMsg: (ReadOnlyOperationReqUserMsg,
                                                  ReadOnlyOperationReqUserMsg_t),
        AnyUserMsgUnion.WriteOnlyOperationReqUserMsg: (WriteOnlyOperationReqUserMsg,
                                                   WriteOnlyOperationReqUserMsg_t)
        }
