# IoT_Database_Node
# Copyright (C) 2020 Szymon Janora

# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <https://www.gnu.org/licenses/>.

from abc import abstractmethod
from message_templates.flatbuffer_message_template \
    import flatbuffer_message_template_t, init_attrs, struct_t
from PyQt5.QtCore import QVariant, QDataStream, QByteArray, QIODevice
from PyQt5.QtNetwork import QHostAddress
from IoTDNP.Msg import *
from IoTDNP.AnyMsgUnion import *
from IoTDNP.DatabaseNodeAddrStruct import *
from IoTDNP.InitJoinDatabaseNodeReqMsg import *
from IoTDNP.InitJoinDatabaseNodeRespMsg import *
from IoTDNP.StartJoinDatabaseNodeMsg import *
from IoTDNP.ReadyJoinDatabaseNodeMsg import *
from IoTDNP.ConnJoinDatabaseNodeReqMsg import *
from IoTDNP.ConnJoinDatabaseNodeRespMsg import *
from IoTDNP.StopJoinDatabaseNodeMsg import *
from IoTDNP.FinishJoinDatabaseNodeMsg import *
from IoTDNP.TransactionIdStruct import *
from IoTDNP.StartTransactionMsg import *
from IoTDNP.ReadyTransactionMsg import *
from IoTDNP.StopTransactionMsg import *


class msg_template_t(flatbuffer_message_template_t):

    __slots__ = ['type_msg']

    def __init__(self, type_msg):
        self.type_msg = type_msg

    def serialize(self, builder):
        msg = self.encode(builder)
        MsgStart(builder)
        MsgAddMsgType(builder, self.type_msg)
        MsgAddMsg(builder, msg)
        return MsgEnd(builder)

    @abstractmethod
    def encode(self, builder):
        pass

    @classmethod
    def deserialize(cls, buf, offset):
        msg_root = Msg.GetRootAsMsg(buf, offset)
        type = msg_root.MsgType()
        bytes = msg_root.Msg().Bytes
        pos = msg_root.Msg().Pos
        if type in MSG_TYPES:
            msg_any, msg_temp = MSG_TYPES[type]
        else:
            raise Exception("Unknow msg type id={}!".format(type))
        msg = msg_any()
        msg.Init(bytes, pos)
        return msg_temp.decode(msg)

    @classmethod
    @abstractmethod
    def decode(cls, buf, offset):
        pass

class DatabaseNodeAddrStruct_t(struct_t):

    __slots__ = ['ipv6_ls', 'ipv6_ms', 'port']

    def __init__(self, ipv6_ls, ipv6_ms, port):
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        return CreateDatabaseNodeAddrStruct(builder,
                self.ipv6_ls, self.ipv6_ms, self.port)

    @classmethod
    def decode(cls, stru):
        return DatabaseNodeAddrStruct_t(stru.Ipv6Ls(),
                    stru.Ipv6Ms(), stru.Port())

    @classmethod
    def create(self, addrIpv6, port):
        addr = QHostAddress(addrIpv6).toIPv6Address()
        addrBytes = ''.join([chr(x) for x in addr]).encode()
        data = QByteArray(addrBytes)
        out = QDataStream(data, QIODevice.ReadOnly)
        out.setByteOrder(QDataStream.LittleEndian)
        ipv6_ls = out.readUInt64()
        ipv6_ms = out.readUInt64()
        return DatabaseNodeAddrStruct_t(ipv6_ls,
                    ipv6_ms, port)

class InitJoinDatabaseNodeReqMsg_t(msg_template_t):

    __slots__ = ['joinNodeId', 'addr']

    def __init__(self, joinNodeId, addr):
        super().__init__(AnyMsgUnion.InitJoinDatabaseNodeReqMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        InitJoinDatabaseNodeReqMsgStart(builder)
        InitJoinDatabaseNodeReqMsgAddJoinNodeId(builder, self.joinNodeId)
        InitJoinDatabaseNodeReqMsgAddAddr(builder, self.addr.encode(builder))
        return InitJoinDatabaseNodeReqMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return InitJoinDatabaseNodeReqMsg_t(
                    msg.JoinNodeId(),
                    DatabaseNodeAddrStruct_t.decode(msg.Addr()))


class InitJoinDatabaseNodeRespMsg_t(msg_template_t):

    __slots__ = ['nodeId', 'addr']

    def __init__(self, nodeId, addr):
        super().__init__(AnyMsgUnion.InitJoinDatabaseNodeRespMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        InitJoinDatabaseNodeRespMsgStart(builder)
        InitJoinDatabaseNodeRespMsgAddNodeId(builder, self.nodeId)
        InitJoinDatabaseNodeRespMsgAddAddr(builder, self.addr.encode(builder))
        return InitJoinDatabaseNodeRespMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return InitJoinDatabaseNodeRespMsg_t(
                    msg.NodeId(),
                    DatabaseNodeAddrStruct_t.decode(msg.Addr()))


class TransactionIdStruct_t(struct_t):

    __slots__ = ['timeMsSinceEpoch', 'nodeId', 'nodeCounter']

    def __init__(self, timeMsSinceEpoch, nodeId, nodeCounter):
        init_attrs(self, locals(), self.__slots__)

    def __eq__(self, other):
        """Overrides the default implementation"""
        if isinstance(other, type(self)):
            if self.nodeId != other.nodeId:
                return False
            if self.nodeCounter != other.nodeCounter:
                return False
            if self.timeMsSinceEpoch != 0 and \
               self.timeMsSinceEpoch != other.timeMsSinceEpoch:
                return False
            return True
        return False

    def encode(self, builder):
        return CreateTransactionIdStruct(builder,
                self.timeMsSinceEpoch, self.nodeId, self.nodeCounter)

    @classmethod
    def decode(cls, stru):
        return TransactionIdStruct_t(stru.TimeMsSinceEpoch(),
                    stru.NodeId(), stru.NodeCounter())


class StartJoinDatabaseNodeMsg_t(msg_template_t):

    __slots__ = ['nodeId', 'transactionId', 'joinNodeId', 'addr', 'nodes']

    def __init__(self, nodeId, transactionId, joinNodeId, addr, nodes):
        super().__init__(AnyMsgUnion.StartJoinDatabaseNodeMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        StartJoinDatabaseNodeMsgStart(builder)
        StartJoinDatabaseNodeMsgAddNodeId(builder, self.nodeId)
        StartJoinDatabaseNodeMsgAddTransactionId(builder, self.transactionId.encode(builder))
        StartJoinDatabaseNodeMsgAddJoinNodeId(builder, self.joinNodeId)
        StartJoinDatabaseNodeMsgAddAddr(builder, self.addr.encode(builder))
        StartJoinDatabaseNodeMsgAddNodes(builder, self.nodes)
        return StartJoinDatabaseNodeMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return StartJoinDatabaseNodeMsg_t(
                    msg.NodeId(), TransactionIdStruct_t.decode(msg.TransactionId()),
                    msg.JoinNodeId(), DatabaseNodeAddrStruct_t.decode(msg.Addr()),
                    msg.Nodes())


class ReadyJoinDatabaseNodeMsg_t(msg_template_t):

    __slots__ = ['nodeId', 'transactionId', 'status']

    def __init__(self, nodeId, transactionId, status):
        super().__init__(AnyMsgUnion.ReadyJoinDatabaseNodeMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        ReadyJoinDatabaseNodeMsgStart(builder)
        ReadyJoinDatabaseNodeMsgAddNodeId(builder, self.nodeId)
        ReadyJoinDatabaseNodeMsgAddTransactionId(builder, self.transactionId.encode(builder))
        ReadyJoinDatabaseNodeMsgAddStatus(builder, self.status)
        return ReadyJoinDatabaseNodeMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return ReadyJoinDatabaseNodeMsg_t(
                    msg.NodeId(), TransactionIdStruct_t.decode(msg.TransactionId()),
                    msg.Status())


class ConnJoinDatabaseNodeReqMsg_t(msg_template_t):

    __slots__ = ['nodeId', 'transactionId', 'addr']

    def __init__(self, nodeId, transactionId, addr):
        super().__init__(AnyMsgUnion.ConnJoinDatabaseNodeRespMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        ConnJoinDatabaseNodeRespMsgStart(builder)
        ConnJoinDatabaseNodeRespMsgAddNodeId(builder, self.nodeId)
        ConnJoinDatabaseNodeRespMsgAddTransactionId(builder, self.transactionId.encode(builder))
        ConnJoinDatabaseNodeRespMsgAddAddr(builder, self.addr.encode(builder))
        return ConnJoinDatabaseNodeRespMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return ConnJoinDatabaseNodeReqMsg_t(
                    msg.NodeId(), TransactionIdStruct_t.decode(msg.TransactionId()),
                    DatabaseNodeAddrStruct_t.decode(msg.Addr()))


class ConnJoinDatabaseNodeRespMsg_t(msg_template_t):

    __slots__ = ['joinNodeId', 'transactionId']

    def __init__(self, joinNodeId, transactionId):
        super().__init__(AnyMsgUnion.ConnJoinDatabaseNodeRespMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        ConnJoinDatabaseNodeRespMsgStart(builder)
        ConnJoinDatabaseNodeRespMsgAddJoinNodeId(builder, self.joinNodeId)
        ConnJoinDatabaseNodeRespMsgAddTransactionId(builder, self.transactionId.encode(builder))
        return ConnJoinDatabaseNodeRespMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return ConnJoinDatabaseNodeRespMsg_t(
                    msg.JoinNodeId(), TransactionIdStruct_t.decode(msg.TransactionId()))


class StopJoinDatabaseNodeMsg_t(msg_template_t):

    __slots__ = ['nodeId', 'transactionId', 'status']

    def __init__(self, nodeId, transactionId, status):
        super().__init__(AnyMsgUnion.StopJoinDatabaseNodeMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        ReadyJoinDatabaseNodeMsgStart(builder)
        ReadyJoinDatabaseNodeMsgAddNodeId(builder, self.nodeId)
        ReadyJoinDatabaseNodeMsgAddTransactionId(builder, self.transactionId.encode(builder))
        ReadyJoinDatabaseNodeMsgAddStatus(builder, self.status)
        return ReadyJoinDatabaseNodeMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return StopJoinDatabaseNodeMsg_t(
                    msg.NodeId(), TransactionIdStruct_t.decode(msg.TransactionId()),
                    msg.Status())


class FinishJoinDatabaseNodeMsg_t(msg_template_t):

    __slots__ = ['transNodes']

    def __init__(self, transNodes):
        super().__init__(AnyMsgUnion.FinishJoinDatabaseNodeMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        FinishJoinDatabaseNodeMsgStart(builder)
        FinishJoinDatabaseNodeMsgAddTransNodes(builder, self.transNodes)
        return FinishJoinDatabaseNodeMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return FinishJoinDatabaseNodeMsg_t(msg.TransNodes())

class StartTransactionMsg_t(msg_template_t):

    __slots__ = ['table', 'transactionId', 'nodeId', 'nodes', 'cmd']

    def __init__(self, table, transactionId, nodeId, nodes, cmd):
        super().__init__(AnyMsgUnion.StartTransactionMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        table = builder.CreateString(self.table)
        cmd = builder.CreateString(self.cmd)
        StartTransactionMsgStart(builder)
        StartTransactionMsgAddTable(builder, table)
        StartTransactionMsgAddTransactionId(builder, self.transactionId.encode(builder))
        StartTransactionMsgAddNodeId(builder, self.nodeId)
        StartTransactionMsgAddNodes(builder, self.nodes)
        StartTransactionMsgAddCmd(builder, cmd)
        return StartTransactionMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return StartTransactionMsg_t(msg.Table().decode(),
                    TransactionIdStruct_t.decode(msg.TransactionId()),
                    msg.NodeId(), msg.Nodes(), msg.Cmd().decode())


class ReadyTransactionMsg_t(msg_template_t):

    __slots__ = ['table', 'transactionId', 'nodeId', 'status']

    def __init__(self, table, transactionId, nodeId, status):
        super().__init__(AnyMsgUnion.ReadyTransactionMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        table = builder.CreateString(self.table)
        ReadyTransactionMsgStart(builder)
        ReadyTransactionMsgAddTable(builder, table)
        ReadyTransactionMsgAddTransactionId(builder, self.transactionId.encode(builder))
        ReadyTransactionMsgAddNodeId(builder, self.nodeId)
        ReadyTransactionMsgAddStatus(builder, self.status)
        return ReadyTransactionMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return ReadyTransactionMsg_t(msg.Table().decode(),
                    TransactionIdStruct_t.decode(msg.TransactionId()),
                    msg.NodeId(), msg.Status())

class StopTransactionMsg_t(msg_template_t):

    __slots__ = ['table', 'transactionId', 'nodeId', 'status']

    def __init__(self, table, transactionId, nodeId, status):
        super().__init__(AnyMsgUnion.StopTransactionMsg)
        init_attrs(self, locals(), self.__slots__)

    def encode(self, builder):
        table = builder.CreateString(self.table)
        StopTransactionMsgStart(builder)
        StopTransactionMsgAddTable(builder, table)
        StopTransactionMsgAddTransactionId(builder, self.transactionId.encode(builder))
        StopTransactionMsgAddNodeId(builder, self.nodeId)
        StopTransactionMsgAddStatus(builder, self.status)
        return StopTransactionMsgEnd(builder)

    @classmethod
    def decode(cls, msg):
        return StopTransactionMsg_t(msg.Table().decode(),
                    TransactionIdStruct_t.decode(msg.TransactionId()),
                    msg.NodeId(), msg.Status())


MSG_TYPES = {
        AnyMsgUnion.InitJoinDatabaseNodeReqMsg: (InitJoinDatabaseNodeReqMsg, InitJoinDatabaseNodeReqMsg_t),
        AnyMsgUnion.InitJoinDatabaseNodeRespMsg: (InitJoinDatabaseNodeRespMsg, InitJoinDatabaseNodeRespMsg_t),
        AnyMsgUnion.StartJoinDatabaseNodeMsg: (StartJoinDatabaseNodeMsg, StartJoinDatabaseNodeMsg_t),
        AnyMsgUnion.ReadyJoinDatabaseNodeMsg: (ReadyJoinDatabaseNodeMsg, ReadyJoinDatabaseNodeMsg_t),
        AnyMsgUnion.ConnJoinDatabaseNodeReqMsg: (ConnJoinDatabaseNodeReqMsg, ConnJoinDatabaseNodeReqMsg_t),
        AnyMsgUnion.ConnJoinDatabaseNodeRespMsg: (ConnJoinDatabaseNodeRespMsg, ConnJoinDatabaseNodeRespMsg_t),
        AnyMsgUnion.StopJoinDatabaseNodeMsg: (StopJoinDatabaseNodeMsg, StopJoinDatabaseNodeMsg_t),
        AnyMsgUnion.FinishJoinDatabaseNodeMsg: (FinishJoinDatabaseNodeMsg, FinishJoinDatabaseNodeMsg_t),
        AnyMsgUnion.StartTransactionMsg: (StartTransactionMsg, StartTransactionMsg_t),
        AnyMsgUnion.ReadyTransactionMsg: (ReadyTransactionMsg, ReadyTransactionMsg_t),
        AnyMsgUnion.StopTransactionMsg: (StopTransactionMsg, StopTransactionMsg_t)
        }
