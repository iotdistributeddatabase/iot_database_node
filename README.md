### Preface
This project's aim is to create a data replication as a part of the distributed IoT system.

### IoT Database Node Project Overview
![Doc/GeneralViewOfReplicationProject.svg](Doc/GeneralViewOfReplicationProject.svg)

The design of this project includes a number of algorithms that resolve concurrent software issues in a heterogeneous distributed data replication system for the IoT network.
The designed data replication algorithms are based on the decentralised three-phase commit protocol (decentralised 3PC) and the decentralised two-phase commit
protocol (decentralised 2PC), where all nodes communicate with each other without a designated coordinator, creating a mesh topology.

The project has three main elements:
* **IoT Database Node App (IoT Database Node)** - an application of the node responsible for data replication in the system;
* **IoT Database Node Driver Plugin** - a plugin for project usage;
* **Database** - a replicated database.

### IoT Database Node Application Architecture
![Doc/ArchitectureIoTDatabaseNode.svg](Doc/ArchitectureIoTDatabaseNode.svg)

The architecture of the data replication system is designed by separating components and interfaces,
to simplify the problem and increase the flexibility of the system.

IoT Database Node components:
* **Replication Service** - a component that handles data replication between system nodes;
* **User Service** - a component that is responsible for handling operations performed on replicated data;
* **Database Driver** - a component that supports database communication;
* **IoT Database Node Driver** - a component for easy communication with the data replication node.

Designed interfaces that interconnect components within the replication architecture:
* **IoTDNP** - a protocol for replication of data between system nodes;
* **IoTDNPUser** - a protocol of operations performed on replicated or replicated user data;
* **ReplicationI** - the replication service interface that allows replication of data for the user service;
* **DatabaseDriverI** - a component interface that supports database communication;
* **IoTDatabaseNodeDriverI** - a component interface for easy communication with the data replication node.

### IoT Database Node Application Implementation
![Doc/ImplementationArchitectureIoTDatabaseNode.svg](Doc/ImplementationArchitectureIoTDatabaseNode.svg)

The current implementation of the developed replication engine variants has been partially implemented with significant changes to take advantage of some of the solutions already available on the development platforms.
The DatabaseDriverI and IoTDatabaseNodeDriverI interfaces have been modified and have been completely changed to the already done QSqlDriver and QSqlDriverPlugin interfaces from the QT development platform.

#### IoT Database Node Application Structure
The purpose of the work-designed application structure is to maximize the scalability of the solution.
The object of the structure analysis was to find an effective solution to the message processing mechanism, which is exchanged in large quantities between nodes.
The exchange of messages between nodes is based on communication using TCP network sockets.
Therefore, any action to send or receive messages on the TCP socket is a low-level I/O operation which it blocks the processing of messages at the business logic level of an application when the same thread handles message processing and I/O operations.
The design of the application structure is based on the proactor pattern to address this issue.

![Doc/DiagramStructureOfApplication.svg](Doc/DiagramStructureOfApplication.svg)

The main application thread receives connections from replication nodes or users
and manages application life.

The application processes messages within three thread pools, such as:
* **Replication Worker Pool** - supports receive/send data replication-related messages;
* **User Worker Pool** - supports receive/send message handling for user-reported replication requests;
* **Thread Pool** - performs message processing business logic tasks for both replication and users.
