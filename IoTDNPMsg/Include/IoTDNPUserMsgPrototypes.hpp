/*
 * IoTDNPMsg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MSG_IOTDNP_USERMSGROTOTYPES_HPP
#define MSG_IOTDNP_USERMSGROTOTYPES_HPP

#include <MessageBuilder.hpp>
#include <IoTDNPUser_generated.h>

namespace Msg
{

template<typename T, typename FUN, typename... Ts>
void MessageBuilder::createMsg(T type, FUN fun, Ts... args)
{
    auto msg = createField(fun, args...);
    createAnyMsg(::IoTDNP::CreateUserMsg, type, msg);
}

namespace IoTDNP
{

class OperationRespUserMsgPrototype : public MessageBuilder
{
public:
    explicit OperationRespUserMsgPrototype(const std::string_view result, const ::IoTDNP::StatusOperationEnum status)
    {
        auto resultStr = createString(result);
        createMsg(::IoTDNP::AnyUserMsgUnion_OperationRespUserMsg,
                  ::IoTDNP::CreateOperationRespUserMsg, resultStr, status);
    }
};

} // namespace IoTDNP
} // namespace Msg

#endif // MSG_IOTDNP_USERMSGROTOTYPES_HPP
