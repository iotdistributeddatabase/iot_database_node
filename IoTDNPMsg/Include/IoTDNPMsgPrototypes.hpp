/*
 * IoTDNPMsg
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef MSG_IOTDNP_MSGPROTOTYPES_HPP
#define MSG_IOTDNP_MSGPROTOTYPES_HPP

#include <MessageBuilder.hpp>
#include <IoTDNP_generated.h>

namespace Msg
{

template<typename T, typename FUN, typename... Ts>
void MessageBuilder::createMsg(T type, FUN fun, Ts... args)
{
    auto msg = createField(fun, args...);
    createAnyMsg(::IoTDNP::CreateMsg, type, msg);
}

namespace IoTDNP
{

class InitJoinDatabaseNodeReqMsgPrototype : public MessageBuilder
{
public:
    explicit InitJoinDatabaseNodeReqMsgPrototype(const uint64_t joinNodeId,
                                                 const ::IoTDNP::DatabaseNodeAddrStruct& addr)
    {
        createMsg(::IoTDNP::AnyMsgUnion_InitJoinDatabaseNodeReqMsg,
                  ::IoTDNP::CreateInitJoinDatabaseNodeReqMsg, joinNodeId, &addr);
    }
};

class InitJoinDatabaseNodeRespMsgPrototype : public MessageBuilder
{
public:
    explicit InitJoinDatabaseNodeRespMsgPrototype(const uint64_t nodeId,
                                                  const ::IoTDNP::DatabaseNodeAddrStruct& addr)
    {
        createMsg(::IoTDNP::AnyMsgUnion_InitJoinDatabaseNodeRespMsg,
                  ::IoTDNP::CreateInitJoinDatabaseNodeRespMsg, nodeId, &addr);
    }
};

class StartJoinDatabaseNodeMsgPrototype : public MessageBuilder
{
public:
    explicit StartJoinDatabaseNodeMsgPrototype(const uint64_t nodeId,
                                               const ::IoTDNP::TransactionIdStruct& transId,
                                               const uint64_t joinNodeId,
                                               const ::IoTDNP::DatabaseNodeAddrStruct* addr,
                                               const uint64_t nodes)
    {
        createMsg(::IoTDNP::AnyMsgUnion_StartJoinDatabaseNodeMsg,
                  ::IoTDNP::CreateStartJoinDatabaseNodeMsg, nodeId, &transId,
                  joinNodeId, addr, nodes);
    }
};

class ReadyJoinDatabaseNodeMsgPrototype : public MessageBuilder
{
public:
    explicit ReadyJoinDatabaseNodeMsgPrototype(const uint64_t nodeId,
                                               const ::IoTDNP::TransactionIdStruct* transId,
                                               const ::IoTDNP::ReadyStatusEnum status)
    {
        createMsg(::IoTDNP::AnyMsgUnion_ReadyJoinDatabaseNodeMsg,
                  ::IoTDNP::CreateReadyJoinDatabaseNodeMsg, nodeId, transId, status);
    }
    explicit ReadyJoinDatabaseNodeMsgPrototype(const uint64_t nodeId,
                                               const ::IoTDNP::TransactionIdStruct& transId,
                                               const ::IoTDNP::ReadyStatusEnum status)
        : ReadyJoinDatabaseNodeMsgPrototype(nodeId, &transId, status)
    {}
};

class ConnJoinDatabaseNodeReqMsgPrototype : public MessageBuilder
{
public:
    explicit ConnJoinDatabaseNodeReqMsgPrototype(const uint64_t nodeId,
                                                 const ::IoTDNP::TransactionIdStruct& transId,
                                                 const ::IoTDNP::DatabaseNodeAddrStruct& addr)
    {
        createMsg(::IoTDNP::AnyMsgUnion_ConnJoinDatabaseNodeReqMsg,
                  ::IoTDNP::CreateConnJoinDatabaseNodeReqMsg, nodeId, &transId, &addr);
    }
};

class ConnJoinDatabaseNodeRespMsgPrototype : public MessageBuilder
{
public:
    explicit ConnJoinDatabaseNodeRespMsgPrototype(const uint64_t joinNodeId,
                                                  const ::IoTDNP::TransactionIdStruct* transId)
    {
        createMsg(::IoTDNP::AnyMsgUnion_ConnJoinDatabaseNodeRespMsg,
                  ::IoTDNP::CreateConnJoinDatabaseNodeRespMsg, joinNodeId, transId);
    }
};

class StopJoinDatabaseNodeMsgPrototype : public MessageBuilder
{
public:
    explicit StopJoinDatabaseNodeMsgPrototype(const uint64_t nodeId,
                                              const ::IoTDNP::TransactionIdStruct* transId,
                                              const ::IoTDNP::CommitStatusEnum status)
    {
        createMsg(::IoTDNP::AnyMsgUnion_StopJoinDatabaseNodeMsg,
                  ::IoTDNP::CreateStopJoinDatabaseNodeMsg, nodeId, transId, status);
    }
    explicit StopJoinDatabaseNodeMsgPrototype(const uint64_t nodeId,
                                              const ::IoTDNP::TransactionIdStruct& transId,
                                              const ::IoTDNP::CommitStatusEnum status)
        : StopJoinDatabaseNodeMsgPrototype(nodeId, &transId, status)
    {
    }
};

class FinishJoinDatabaseNodeRespMsgPrototype : public MessageBuilder
{
public:
    explicit FinishJoinDatabaseNodeRespMsgPrototype(const uint64_t transNodes)
    {
        createMsg(::IoTDNP::AnyMsgUnion_FinishJoinDatabaseNodeMsg,
                  ::IoTDNP::CreateFinishJoinDatabaseNodeMsg, transNodes);
    }
};

class FetchDatabaseStateReqMsgBuilder : public MessageBuilder
{
public:
    explicit FetchDatabaseStateReqMsgBuilder(const uint64_t nodeId, const bool subscribe)
    {
        createMsg(::IoTDNP::AnyMsgUnion_FetchDatabaseStateReqMsg,
                  ::IoTDNP::CreateFetchDatabaseStateReqMsg, nodeId, subscribe);
    }
};

class FetchDatabaseStateRespMsgBuilder : public MessageBuilder
{
public:
    explicit FetchDatabaseStateRespMsgBuilder() = default;
    void createUninitializedVector(const std::size_t size)
    {
        tableStates_.reserve(size);
    }
    void pushTableState(const char* table, const uint64_t tableTransId)
    {
        auto tableState = createField(::IoTDNP::CreateTableStateTableDirect, table, tableTransId);
        tableStates_.push_back(tableState);
    }
    void finish(const ::IoTDNP::TransactionIdStruct& transactionId)
    {
        auto tableStates = createVector(tableStates_);
        createMsg(::IoTDNP::AnyMsgUnion_FetchDatabaseStateRespMsg,
                  ::IoTDNP::CreateFetchDatabaseStateRespMsg, &transactionId, tableStates);
    }
private:
    typedef flatbuffers::Offset<::IoTDNP::TableStateTable> TypeTableStateTable;
    std::vector<TypeTableStateTable> tableStates_ {};
};

class OverflowHistoryFileMsgBuilder : public MessageBuilder
{
public:
    explicit OverflowHistoryFileMsgBuilder(const uint8_t* const fileBuf, const std::size_t size)
    {
        auto file = createVector(fileBuf, size);
        createMsg(::IoTDNP::AnyMsgUnion_OverflowHistoryFileMsg,
                  ::IoTDNP::CreateOverflowHistoryFileMsg, file);
    }
};

class UnsubscribeOverflowHistoryFileMsgBuilder : public MessageBuilder
{
public:
    explicit UnsubscribeOverflowHistoryFileMsgBuilder(const uint64_t nodeId)
    {
        createMsg(::IoTDNP::AnyMsgUnion_UnsubscribeOverflowHistoryFileMsg,
                  ::IoTDNP::CreateUnsubscribeOverflowHistoryFileMsg, nodeId);
    }
};

class FetchDatabaseTableReqMsgBuilder : public MessageBuilder
{
public:
    explicit FetchDatabaseTableReqMsgBuilder(const std::string_view table)
    {
        auto tableStr = createString(table);
        createMsg(::IoTDNP::AnyMsgUnion_FetchDatabaseTableReqMsg,
                  ::IoTDNP::CreateFetchDatabaseTableReqMsg, tableStr);
    }
};

class FetchDatabaseTableRespMsgBuilder : public MessageBuilder
{
public:
    explicit FetchDatabaseTableRespMsgBuilder(const std::string_view table,
                                              const std::vector<std::string>& columns,
                                              const uint64_t rows,
                                              const uint8_t* const records,
                                              const std::size_t sizeRecords)
    {
        auto tableStr = createString(table);
        auto columnsVector = createVectorOfStrings(columns);
        auto recordsVector = createVector(records, sizeRecords);
        createMsg(::IoTDNP::AnyMsgUnion_FetchDatabaseTableRespMsg,
                  ::IoTDNP::CreateFetchDatabaseTableRespMsg,
                  tableStr, columnsVector, rows, recordsVector);
    }
};

class StartTransactionMsgPrototype : public MessageBuilder
{
public:
    explicit StartTransactionMsgPrototype(const std::string_view table,
                                          const ::IoTDNP::TransactionIdStruct* transId,
                                          const uint64_t nodeId,
                                          const uint64_t nodes,
                                          const std::string_view cmd)
    {
        auto tableStr = createString(table);
        auto cmdStr = createString(cmd);
        createMsg(::IoTDNP::AnyMsgUnion_StartTransactionMsg,
                  ::IoTDNP::CreateStartTransactionMsg,
                  tableStr, transId, nodeId, nodes, cmdStr);
    }
};

class ReadyTransactionMsgPrototype : public MessageBuilder
{
public:
    explicit ReadyTransactionMsgPrototype(const std::string_view table,
                                          const ::IoTDNP::TransactionIdStruct* transId,
                                          const uint64_t nodeId,
                                          const ::IoTDNP::ReadyStatusEnum status)
    {
        auto tableStr = createString(table);
        createMsg(::IoTDNP::AnyMsgUnion_ReadyTransactionMsg,
                  ::IoTDNP::CreateReadyTransactionMsg,
                  tableStr, transId, nodeId, status);
    }
};

class StopTransactionMsgPrototype : public MessageBuilder
{
public:
    explicit StopTransactionMsgPrototype(const std::string_view table,
                                         const ::IoTDNP::TransactionIdStruct& transId,
                                         const uint64_t nodeId,
                                         const ::IoTDNP::CommitStatusEnum status)
    {
        auto tableStr = createString(table);
        createMsg(::IoTDNP::AnyMsgUnion_StopTransactionMsg,
                  ::IoTDNP::CreateStopTransactionMsg,
                  tableStr, &transId, nodeId, status);
    }
};

} // namespace IoTDNP
} // namespace Msg

#endif // MSG_IOTDNP_MSGPROTOTYPES_HPP
