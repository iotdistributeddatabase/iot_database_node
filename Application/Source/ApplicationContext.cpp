/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include "ApplicationContext.hpp"
#include <QSettings>
#include <QDebug>
#include <QTextCodec>
#include <QHostAddress>
#include <DbConnectionPool.hpp>
#include <QThreadPool>
#include <QtSql/QSqlQuery>
#include <QtSql/QSqlError>
#include <QtSql/QSqlRecord>
#include <QByteArray>
#include <QDataStream>
#include <QVariant>
#include <QVariantList>

ApplicationContext::ApplicationContext(const QString configFilePath)
{
    QSettings settings(configFilePath, QSettings::IniFormat);
    settings.setIniCodec(QTextCodec::codecForName("UTF-8"));
    settings.sync();

    qDebug() << "Conqueryfig file child keys =" << settings.childKeys();
    qDebug() << "Config file group keys =" << settings.childGroups();
    //settings.beginGroup("General"); TODO: Add General group in the settings file
    QString addr = settings.value("ListenAddress", "::1").toString();
    QString port = settings.value("ListenPort", "9000").toString();
    //settings.endGroup();
    qInfo() << "User ListenAddress =" << addr << "ListenPort =" << port;

    settings.beginGroup("Replication");
    QString replicAddr = settings.value("ListenAddress", "::1").toString();
    QString replicPort = settings.value("ListenPort", "8000").toString();
    std::size_t numOfWorkers = static_cast<std::size_t >(settings.value("NumOfWorkers", 4).toUInt());
    uint64_t nodeId = static_cast<uint64_t>(settings.value("NodeIdentifier", 0).toUInt());
    QString joinAddrStr = settings.value("JoinAddress", "").toString();
    QString joinPortStr = settings.value("JoinPort", "").toString();
    settings.endGroup();
    qInfo() << "Replication ListenAddress =" << replicAddr << "ListenPort ="
            << replicPort << "NumOfWorkers =" << numOfWorkers
            << "NodeIdentifier =" << nodeId
            << "JoinAddress =" << joinAddrStr << "JoinPort =" << joinPortStr;

    settings.beginGroup("Database");
    QString driverType = settings.value("DriverType", "").toString();
    QString databaseName = settings.value("DatabaseName", "").toString();
    QString hostName = settings.value("HostName", "").toString();
    uint16_t portDb = static_cast<uint16_t>(settings.value("Port", 0).toUInt());
    QString userName = settings.value("UserName", "").toString();
    QString password = settings.value("Password", "").toString();
    QString connectOptions = settings.value("ConnectOptions", "").toString();
    settings.endGroup();
    qInfo() << "Database DriverType =" << driverType
            << "DatabaseName =" << databaseName << "HostName =" << hostName
            << "PortDb =" << portDb << "UserName =" << userName
            << "Password =" << password << "ConnectOptions =" << connectOptions;

    Database::DbConnectionPool::createGlobalInstance(driverType,
                                                     databaseName,
                                                     hostName,
                                                     portDb,
                                                     userName,
                                                     password,
                                                     connectOptions);

    QHostAddress ipAddrReplic{replicAddr};
    replicationServer_ = std::make_unique<ReplicationService::ReplicationServer>(
                ipAddrReplic, replicPort.toUInt(), numOfWorkers, nodeId);
    replicationServer_->startServer();
    if(not joinPortStr.isEmpty())
    {
        QHostAddress joinAddr {joinAddrStr};
        replicationServer_->joinDatabaseNode(joinAddr,
                                             static_cast<uint16_t>(joinPortStr.toUInt()));
    }
    QHostAddress ipAddr {addr};
    userServer_ = std::make_unique<UserService::UserServer>(ipAddr, port.toUInt(), 1);
    userServer_->startServer();
}

ApplicationContext::~ApplicationContext()
{
    qDebug() << "ApplicationContext was destroyed!";
}

void ApplicationContext::handleExitApplication()
{
    qCritical("Handle exit appliaction");
    userServer_->stopServer();
    userServer_.reset();
    replicationServer_->stopServer();
    replicationServer_.reset();
    QThreadPool::globalInstance()->clear();
    Database::DbConnectionPool::globalInstance()->clear();
}

