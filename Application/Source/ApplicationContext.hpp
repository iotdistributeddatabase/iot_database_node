/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef APPLICATIONCONTEXT_HPP
#define APPLICATIONCONTEXT_HPP

#include <QObject>
#include <QCoreApplication>
#include <UserServer.hpp>
#include <ReplicationServer.hpp>
#include <memory>

class ApplicationContext : public QObject
{
    Q_OBJECT
public:
    explicit ApplicationContext(const QString configFilePath);
    virtual ~ApplicationContext();

public slots:
    void handleExitApplication();

private:
    std::unique_ptr<ReplicationService::ReplicationServer> replicationServer_;
    std::unique_ptr<UserService::UserServer> userServer_;
};

#endif // APPLICATIONCONTEXT_HPP
