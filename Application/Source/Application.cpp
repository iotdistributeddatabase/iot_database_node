/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#include <PostMortem/CrashHandler.hpp>
#define FLATBUFFERS_TRACK_VERIFIER_BUFFER_SIZE
#include <QtDebug>
#include <QDateTime>
#include <QCoreApplication>
#include <csignal>
#include <QObject>
#include <QCommandLineParser>
#include <QFile>
#include "ApplicationContext.hpp"

void logFormatOutput(QtMsgType type, const QMessageLogContext &context, const QString &msg)
{
    QByteArray localMsg = msg.toUtf8();
    const char* log = localMsg.constData();
    const char* file = context.file ? context.file : "";
    const char* function = context.function ? context.function : "";
    int line = context.line;
    QString formatT = "dd.MM.yyyyThh:mm:ss.zzzzzzzzzt";
    const char* t = QDateTime::currentDateTimeUtc().toString(formatT).toUtf8().constData();
    switch (type) {
    case QtDebugMsg:
        fprintf(stderr, "%s Debug: %s (%s:%u, %s)\n", t, log, file, line, function);
        break;
    case QtInfoMsg:
        fprintf(stderr, "%s Info: %s (%s:%u, %s)\n", t, log, file, line, function);
        break;
    case QtWarningMsg:
        fprintf(stderr, "%s Warning: %s (%s:%u, %s)\n", t, log, file, line, function);
        break;
    case QtCriticalMsg:
        fprintf(stderr, "%s Critical: %s (%s:%u, %s)\n", t, log, file, line, function);
        break;
    case QtFatalMsg:
        fprintf(stderr, "%s Fatal: %s (%s:%u, %s)\n", t, log, file, line, function);
        break;
    }
}

void signalHandler(int signum)
{
    qCritical() << "Handle system signal signum =" << signum;
    QCoreApplication::exit(signum);
}

int main(int argc, char *argv[])
{
    // register crash handler
    PostMortem::registerCrashHandler();
    // register signal SIGINT and signal handler
    std::signal(SIGINT, signalHandler);
    // register signal SIGTERM and signal handler
    std::signal(SIGTERM, signalHandler);

    qInstallMessageHandler(logFormatOutput);

    QCoreApplication app(argc, argv);
    app.setApplicationName("IoT_Database_Node");
    QCoreApplication::setApplicationVersion("0.0.0");

    QCommandLineParser parser;
    parser.setApplicationDescription("IoT_Database_Node application");
    parser.addHelpOption();
    parser.addVersionOption();
    parser.addPositionalArgument("config_file_path", "Configuration file with the parameters and initial settings for application.");
    parser.process(app);

    const QStringList args = parser.positionalArguments();
    qInfo() << "args =" << args;
    if(args.size() != 1)
    {
        qCritical() << "Missing argument: path to Configuration file!";
        return -1;
    }
    QString configFilePath = args.at(0);
    if(!QFile(configFilePath).exists())
    {
        qCritical() << "Configuration file does not exist!";
        return -1;
    }

    ApplicationContext appContext{configFilePath};
    QObject::connect(&app, SIGNAL(aboutToQuit()), &appContext, SLOT(handleExitApplication()));

    const auto retval =  app.exec();
    qCritical() << "Application return code =" << retval;
    return retval;
}
