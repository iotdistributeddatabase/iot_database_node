/*
 * IoT_Database_Node
 * Copyright (C) 2020 Szymon Janora
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

#ifndef UTIL_HANDLER_HANDLERTASK_HPP
#define UTIL_HANDLER_HANDLERTASK_HPP

#include <QRunnable>
#include <memory>
#include <MessageBuffer.hpp>


namespace Util
{
namespace Handler
{

class HandlerTask : public QRunnable
{
    using MsgType = void;
    using MsgHandleType = void;
};

template <typename T, typename MsgWrapperType, typename... Args>
std::unique_ptr<T> makeHandlerTask(Msg::MessageBuffer&& msgBuf,
                                   const MsgWrapperType* msg,
                                   Args&&... args)
{
    using MsgType = typename T::MsgType;
    using MsgHandleType = typename T::MsgHandleType;
    return std::make_unique<T>(
                MsgHandleType{std::move(msgBuf), msg->template msg_as<MsgType>()},
                std::forward<Args>(args)...);
}

} // namespace Handler
} // namespace Util

#endif // UTIL_HANDLER_HANDLERTASK_HPP
